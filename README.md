RTFoundation
=================

*RTFoundation* is a collection of commonly used classes and utilities, speeding up development of new projects by providing existing tried-and-tested solutions for you to plug into your applications. All are written in Swift.

This pod is an ongoing project. During the development of new apps, or the maintenance of existing projects, feel free to refactor repeating patterns into this pod to share the love.


## Installation

To grab all of *RTFoundation*, simply include the root Pod in your application’s `Podfile`:

```objective-c
pod ‘RTFoundation’, :git => ‘git@bitbucket.org:rowant1990/rtfoundation.git’
```

Alternatively, if you don’t need all of *RTFoundation* and only care about specific components (e.g. Network), you can include the subspec instead:

```objective-c
pod ‘RTFoundation/Network’, :git => ‘git@bitbucket.org:rowant1990/rtfoundation.git’
```

RTFoundation has been updated to be fully compatible with Swift 4, if you need to use as older version of Swift make sure you set the branch in your podfile e.g

```objective-c
pod ‘RTFoundation/Network’, :git => ‘git@bitbucket.org:rowant1990/rtfoundation.git’. :branch => 'swift2.3'
```
