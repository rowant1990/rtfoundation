Pod::Spec.new do |s|
  s.name         = 'RTFoundation'
  s.version      = '4.0.15'
  s.summary      = 'A collection of common classes, categories and utilities all written in swift.'
  s.homepage     = 'https://bitbucket.org/rowant1990/rtfoundation'
  s.license      = 'MIT'
  s.authors      = { 'Rowan Townshend' => 'rowant1990@gmail.com' }
  s.ios.deployment_target = '8.0'
  s.tvos.deployment_target = '9.0'
  s.source       = { :git => 'git@bitbucket.org:rowant1990/rtfoundation.git',
                     :tag => s.version.to_s }
  s.requires_arc = true
  
  s.platforms =  {
      :ios => '8.0',
      :osx => '10.9',
      :tvos => '9.0'
    }
    
  s.subspec 'Networking' do |networking|
    networking.name = 'Networking'
    networking.source_files = 'RTFoundation/Networking/**/*.{swift,h,m}'
    networking.dependency 'RTFoundation/Mapper'
  end
  
  s.subspec 'Mapper' do |mapper|
    mapper.name = 'Mapper'
    mapper.source_files = 'RTFoundation/Mapper/**/*.{swift}'
  end

  s.subspec 'UIKit' do |location|
    location.name = 'UIKit'
    location.source_files = 'RTFoundation/UIKit/**/*.{swift}'
  end
  
  s.subspec 'TableViewDataSource' do |form|
    form.name = 'TableViewDataSource'
    form.source_files = 'RTFoundation/TableViewDataSource/**/*.{swift}'
  end
  
  s.subspec 'APIMock' do |mock|
    mock.name = 'APIMock'
    mock.weak_framework = "XCTest"
    mock.source_files = 'RTFoundation/APIMock/**/*.{swift,m}'
    mock.pod_target_xcconfig = {
      'ENABLE_BITCODE' => 'NO',
      'OTHER_LDFLAGS' => '-weak-lswiftXCTest',
      'FRAMEWORK_SEARCH_PATHS' => '$(inherited) "$(PLATFORM_DIR)/Developer/Library/Frameworks"'
    }
  end
    
end
