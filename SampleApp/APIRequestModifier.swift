//
//  APIRequestModifier.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 09/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class APIRequestModifier: NetworkRequestModifier {
  
  override func configureRequest(_ request: URLRequest) -> URLRequest {
    if let httpBody = request.httpBody, let body = try! JSONSerialization.jsonObject(with: httpBody, options: .allowFragments) as? [String: Any] {
      var queryString = ""
      for (key, value) in body {
        if let value = value as? String, let encodedString = value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)  {
          queryString.append("\(key)=\(encodedString)&")
        } else {
        queryString.append("\(key)=\(value)&")
        }
      }
      var newrequst = request
      newrequst.httpBody =  queryString.data(using: .ascii, allowLossyConversion: true)
      return newrequst
    }
    return request
  }
}
