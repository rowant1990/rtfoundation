//
//  TextFieldTableViewCell.swift
//  RTFoundation
//
//  Created by Rowan on 12/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class TextFieldTableViewCell: UITableViewCell, UITextFieldDelegate {
  
  @IBOutlet var field: UITextField!
  @IBOutlet var titleLabel: UILabel!

  
  func configure(withModel: Any) {
    
  }
  
}
