//
//  ForceTouchDetailedViewController.swift
//  RTFoundation
//
//  Created by Rowan on 10/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class ForceTouchDetailedViewController: UIViewController {
  
  @IBOutlet var mainTitleLabel: UILabel!
  @IBOutlet var subTitle: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
}
