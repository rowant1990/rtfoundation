//
//  BookingSummary.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 27/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation
//import ObjectMapper

struct BookingSummary: Mappable {
  
  var name: String = ""
  var bookings: [Booking]?

  init?(map: Map) {
    
  }
  
  mutating func mapping(map: Map) {
    bookings <- map["Bookings"]
  }
}



