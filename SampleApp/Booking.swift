//
//  Booking.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 27/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation
//import ObjectMapper

struct Booking: Mappable {
  
  init?(map: Map) {
    
  }

  var id: String? = ""
  var type = ""
  var phenixBookingId = ""
  var journeySubtitle = ""
  var journeyTitle = ""
  var departureDate: Date?
  var returnDate: Date?
  var isBusinessBooking = true
  var imageURL: URL?
  var lastUpdated = Date()
  var lastUpdatedAsTimeAgo = ""
  var companyId = ""
  
  mutating func mapping(map: Map) {
    id <- map["Id"]
    isBusinessBooking <- map["IsBusinessBooking"]
    type <- map["Type"]
    phenixBookingId <- (map["PhenixBookingId"])
    departureDate <- (map["DepartureDate"], ISO8601DateTransform())
   // type <- map["Type"]
  }
}


//open class NumberTransform: TransformType  {
//  
//  public typealias Object = String
//  public typealias JSON = Int
//  
//  public init() {}
//  
//  open func transform(from value: Any?) -> String? {
//    guard let value = value else { return nil }
//    return String(describing: value)
//  }
//  
//  open func transform(to value: String?) -> Int? {
//    guard let data = value else{
//      return nil
//    }
//    return nil
//  }
//}
