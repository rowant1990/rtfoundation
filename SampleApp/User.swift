//
//  User.swift
//  RTFoundation
//
//  Created by Rowan on 24/02/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

class User: NSObject {
  
  var name: String?
  var username = ""
  var email: String? = nil
  var url: URL?
  var website: URL? = nil
  
  var address: Address?
  var company: Company? = nil
}
