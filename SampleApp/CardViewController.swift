//
//  CardViewController.swift
//  RTFoundation
//
//  Created by Rowan on 11/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class CardViewController: UIViewController {
  
  var frontController: CardFaceViewController? {
    didSet {
      frontController?.cardViewController = self
    }
  }
  var backController: CardFaceViewController? {
    didSet {
      backController?.cardViewController = self
    }
  }
  
  @IBOutlet var containterView: UIView!
  
  override open func viewDidLoad() {
    
    super.viewDidLoad()
    
    if let frontController = self.frontController {
      self.addChildViewController(frontController)
      self.view.addSubview(frontController.view)
      frontController.didMove(toParentViewController: self)
    }
  }
  
  //MARK: - Public Methods
  
  func cardViewFlip(_ front: Bool) {
    let presenting: CardFaceViewController! = front ?  self.frontController : self.backController
    let presented: CardFaceViewController! = front ?  self.backController : self.frontController
    
    presenting.removeFromParentViewController()
    self.addChildViewController(presented)
    self.view.addSubview(presented.view)
    
    UIView.transition(with: self.view, duration: 1.0, options:front ? .transitionFlipFromRight : .transitionFlipFromLeft, animations: { () -> Void in
      presented.view.frame = self.view.frame
      }) { (finished) -> Void in
        presented.didMove(toParentViewController: self)
        presenting.view.removeFromSuperview()
        presenting.didMove(toParentViewController: nil)
    }
  }
  
}
