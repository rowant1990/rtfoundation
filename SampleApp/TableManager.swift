//
//  TableManager.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 27/07/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

class TableManager: NSObject {
  
  
  

}

protocol FormViewManageable {
  
  var tableView: UITableView! { get }
}

private var DTTableViewManagerAssociatedKey = "DTTableViewManager Associated Key"

extension FormViewManageable {
  
  var manager: TableManager {
    get {
      var object = objc_getAssociatedObject(self, &DTTableViewManagerAssociatedKey)
      if object == nil {
        object = TableManager()
        objc_setAssociatedObject(self, &DTTableViewManagerAssociatedKey, object, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      }
      return object as! TableManager
    }
    set {
      objc_setAssociatedObject(self, &DTTableViewManagerAssociatedKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }

  }
}
