//
//  ControllerFlipViewController.swift
//  RTFoundation
//
//  Created by Rowan on 11/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == "CardContainerSegue" {
      let destination = segue.destination as! CardContainerViewController
      destination.cards = [self.newColoredViewController(UIColor.red),
        self.newColoredViewController(UIColor.blue),
        self.newColoredViewController(UIColor.yellow)]
    }
  }
  
  func newColoredViewController(_ color: UIColor) -> CardViewController {
    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
    let contoller = storyBoard.instantiateViewController(withIdentifier: "CardViewControllerIdentifier") as! CardViewController
    contoller.frontController = storyBoard.instantiateViewController(withIdentifier: "CardFrontViewControllerIdentifier") as! CardFrontViewController
    contoller.backController = storyBoard.instantiateViewController(withIdentifier: "CardBackViewControllerIdentifier") as! CardBackViewController
    contoller.view.backgroundColor = color
    return contoller
  }
  
}
