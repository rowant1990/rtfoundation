//
//  ChatViewController.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 02/03/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import Starscream
import SWXMLHash

class ChatViewController: UIViewController, WebSocketDelegate, MessageDelegate {
  
  @IBOutlet var field: UITextField!

  var socket: WebSocket!
  var messageReciever: MessageReceiver!
  var messageID = 0
  var threadID = 0
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    messageReciever = MessageReceiver()
    messageReciever.delegate = self
    
    socket = WebSocket(url: URL(string: "wss://chat.2peasapp.com:7443/ws/server")!)
    socket.selfSignedSSL = true
    socket.delegate = self
    socket.headers["Sec-WebSocket-Protocol"] = "xmpp"
    socket.connect()
  }
  
  func nextMessageID() -> Int {
    messageID += 1
    return messageID
  }
  
  func open() -> String {
    let host = "chat.2peasapp.com"
    return String("<open xmlns=\"urn:ietf:params:xml:ns:xmpp-framing\" to=\"\(host)\" version=\"1.0\" />")
  }
  
  func auth() -> String {
    return String("<auth mechanism=\"DIGEST-MD5\" xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\"></auth>")
  }
  
  func bind(withResource resource: String) -> String {
    return "<iq id=\"\(nextMessageID())\" type=\"set\"><bind xmlns=\"urn:ietf:params:xml:ns:xmpp-bind\"><resource>\(resource)</resource></bind></iq>";
  }
  
  //MARK - Socket Handler
  
  func websocketDidConnect(_ socket: WebSocket) {
    print("connect")
    
    socket.writeString(open())
  }
  
  func websocketDidDisconnect(_ socket: WebSocket, error: NSError?) {
    print("disconnect")
    
  }
  
  func websocketDidReceiveData(_ socket: WebSocket, data: Data) {
    print("receive")
  }
  
  func websocketDidReceiveMessage(_ socket: WebSocket, text: String) {
    print("message \(text)")
    messageReciever.updateMessage(text)
    
  }
  
  @IBAction func sendMessage(_ sender: UIButton) {
    let to = "2054_andy_jennings@chat.2peasapp.com" //chat username + service name
    let from = "2053_tester_tester"                 //recipient's REAL NAME
    let thread = "1"
    socket.writeString("<message xmlns=\"jabber:client\" id=\"\(nextMessageID())\" to=\"\(to)\" from=\"\(from)\" type=\"chat\"><body>\(field.text)</body><thread>\(thread)</thread><x xmlns=\"jabber:x:event\"><offline/><composing/></x></message>")
    }
  
  //MARK - Message Delegate
  
  func messageOpen() {
    socket.writeString(auth())
  }
  
  func messageAuth() {
    
  }
  
  func messageChallenge(_ challenge: Challenge) {
    
    let nc = "00000001"
    let clientNonce =  NSUUID().UUIDString.stringByReplacingOccurrencesOfString("-", withString: "").lowercased()
    let authzid = "2053_tester_tester"; //Sender's Chat Username
    let digestUri = "xmpp/chat.2peasapp.com"
    let response = AuthResponse.compile(authzid, password: "chat7w/b8IQ("/*Standard Chat Password*/, clientNonce: clientNonce, authzid: authzid, digestUri: digestUri, nc: nc, challenge: challenge)
    
    let seriveName = "chat.2peasapp.com"
    print(response)
    
    let content = "username=\"\(authzid)\",realm=\"\(challenge.realm)\",nonce=\"\(challenge.nonce)\",cnonce=\"\(clientNonce)\",nc=\(nc),qop=\(challenge.qop),digest-uri=\"xmpp/\(seriveName)\",response=\(response),charset=utf-8,authzid=\"\(authzid)\""
    print("Message = \(content)")
    
    let bytes = content.data(using: String.Encoding.utf8)!
    let resp =  "<response xmlns=\"urn:ietf:params:xml:ns:xmpp-sasl\">\((bytes.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))))</response>"
    socket.writeString(resp)
  }
  
  func messageAuthSucesss() {
    socket.writeString(bind(withResource: "Chat"))
  }
  
  func messageIq(_ message: IQMessage) {
    if message.jid.characters.count != 0 {
      socket.writeString("<iq type=\"set\" id=\"\(nextMessageID())\"><session xmlns=\"urn:ietf:params:xml:ns:xmpp-session\"/></iq>");
    } else {
      socket.writeString("<presence id=\"\(nextMessageID())\"><status>Online</status><priority>1</priority></presence>")
    }
  }
  
}

class MessageReceiver {
  
  var delegate: MessageDelegate!
  
  func updateMessage(_ message: String) {
    
    if message.hasPrefix("<open") {
      delegate.messageOpen()
    }
    
    if (message.hasPrefix("<stream:features") && message.range(of: "<mechanism>DIGEST-MD5</mechanism>") != nil) {
      delegate.messageAuth()
    }
    
    if message.hasPrefix("<challenge") {
      if let message = message.sliceFrom(">", to: "<") {
        let data = Data(base64Encoded: message, options: NSData.Base64DecodingOptions(rawValue: 0))
        let challenge = String(data: data!, encoding: String.Encoding.utf8)
        var dict = [String: String]()
        for section in challenge!.components(separatedBy: ",") {
          let rows = section.components(separatedBy: "=")
          dict[rows.first!] = rows.last!.replacingOccurrences(of: "\"", with: "", options: .literal, range: nil)
        }
        delegate.messageChallenge(Challenge(fromDict: dict))
      }
    }
    
    if message.hasPrefix("<success") {
      delegate.messageAuthSucesss()
    }
    
    if message.hasPrefix("<iq") {
      let xml = SWXMLHash.parse(message)
      let session = xml["iq"].element?.attributes["id"]
      let jid = xml["iq"]["bind"]["jid"].element
      
      if let session = session {
        let iqmessage = IQMessage(fromSession: session)
        if let jid = jid {
          iqmessage.jid = jid.text!
        }
        delegate.messageIq(iqmessage)
      }
    }
  }
}

protocol MessageDelegate {
  
  func messageOpen()
  func messageAuth()
  func messageChallenge(_ challenge: Challenge)
  func messageAuthSucesss()
  func messageIq(_ message: IQMessage)
}

extension String {
  func sliceFrom(_ start: String, to: String) -> String? {
    return (range(of: start)?.upperBound).flatMap { sInd in
      (range(of: to, range: sInd..<endIndex)?.lowerBound).map { eInd in
        substring(with: sInd..<eInd)
      }
    }
  }
}
