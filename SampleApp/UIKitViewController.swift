//
//  UIKitViewController.swift
//  RTFoundation
//
//  Created by Rowan on 25/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class UIKitViewController: UIViewController {
  
  @IBOutlet var hexBackground: UIView!
  @IBOutlet var rgbBackground: UIView!
  
  override func viewDidLoad() {
  
    let color = UIColor(rgb: [ 120, 30, 20 ])
    self.rgbBackground.backgroundColor = color
    self.hexBackground.backgroundColor = UIColor(hex:"124124")
    print(color.cgColor)
    print(UIColor(red: 120/255, green: 30/255, blue: 20/255, alpha: 1).cgColor)

  }

}
