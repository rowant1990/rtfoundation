//
//  COBApi.swift
//  CardOneBanking
//
//  Created by Rowan on 01/03/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

//class COBApi<T>: APIConfig<T> {
//  class func request(_ target: COB, parameters: [String: AnyObject]? = nil, completion: @escaping (T?, NSError?) -> Void) {
//    super.request(target, parameters: parameters, completion: completion)
//  }
//}

enum COB {
  case posts
  case login
}

enum COBMethod: String {
  case GET, POST
}

extension COB: APIConfigType {
  
  var baseURL: String { return "https://mytcdev.travelcounsellors.com/OpsQA/" }
  
  var path: String {
    switch self {
    case .posts: return "api/v3/Booking/List"
    case .login: return "api/account/login"
    }
  }
  
  var headerFields: [String: String]? { return ["Authorization" : "bearer bsEclTmFO_PMhpSXS-0YB6EHM6Etv3ORpe0axznqbUKAHokwRsMG67x-6p3zwzyamsxrqMuVVXvSAlIjiz1qa9XbUc67U-GWiQOQoieTV2B9QzPX0xZBlGFQukAOJB2cK9WKQrlJlb7MDKHC7PziCF9lhv3iYRJCeyQdxVsKD2mZ3_bgjBESSckqRhCb9ojhzaTAuEx0LBOnQ_v_xhXcEIZAaVCc26lzLU-mg4L9N512wY2HMXzx6nOuDsdFGtUqeHwFwwqX1kQMOolmzjYS6ANybawsLKuAFjpoNnzOfj3rqXEkgG6_1Sjs4S9QbeTudnlFv20ytSY5VbuQU2kwIEW-sucNps19cKe8PEygERNCfIVvtwUbjpkDzafw14vGq65up2kAKVbg9nndrsYYey0j5FSlLoSqy3Jf797wtsCWBBHInnNU7ZPFwhMAHPL4uuCe-fgF_4utYJ-r-4AVhBeAMQqUgk4Hp6vMUfh0MC0hPBpsoXWcXwoIcMJcZlK850tUgibYynIdhUe7Br8vc0VxOllZPHB7lZX3BhYzDilFOd66BWXlJl53Z7s5TLuqiSEG9FQ3EVrWUJVW_rJbIw",   "Content-Type" : "application/json"]}
  
  var method: String {
    return COBMethod.GET.rawValue
  }
  
  var mapping: NetworkMappingConfig? {
    switch self {
    case .posts: return NetworkMappingConfig(fromRoot: BookingSummary.self)
    case .login: return NetworkMappingConfig(fromPath: "login")
    }
  }
  
  var errorClient: NetworkErrorHandler? {
    return APIErrorHandler()
  }
  
  var requestModifier: NetworkRequestModifier? {
    return APIRequestModifier()
  }
  
  var paramerters: [String : Any]? {
    return nil
  }
}
