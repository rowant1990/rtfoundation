//
//  ViewController.swift
//  SampleApp
//
//  Created by Rowan on 18/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class ListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet fileprivate var tableView: UITableView?
  var sections = [ ["name" : "Network", "identifier" : "NetworkViewControllerIdentifier" ],
    ["name" : "UIKit", "identifier" : "UIKitViewControllerIdentifier"],
    ["name" : "3D Touch", "identifier" : "ForceTouchViewControllerIdentifier"],
    ["name": "Card Flip", "identifier" : "HomeViewControllerIdentifier"],
    ["name" : "Form", "identifier" : "FormViewTwoControllerIdentifier"],
    ["name" : "Chat", "identifier" : "ChatViewControllerIdentifier"],
    ["name" : "Analytics", "identifier" : "AnalyticsViewControllerIdentifier"]]
  
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }
  
  
  //MARK: TableView Delegate
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let identifier = "CellIdentifier"
    var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
    }
    cell?.textLabel?.text = self.sections[(indexPath as NSIndexPath).row]["name"]
    return cell!
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let vc = self.storyboard?.instantiateViewController(withIdentifier: self.sections[(indexPath as NSIndexPath).row]["identifier"]!)
    if let unwrappedVc = vc {
      self.navigationController?.title = self.sections[(indexPath as NSIndexPath).row]["name"]
      self.navigationController?.pushViewController(unwrappedVc, animated: true)
    }
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return self.sections.count
  }
  
  
}

