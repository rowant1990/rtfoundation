//
//  Namespace.swift
//  NetworkTest
//
//  Created by Rowan on 11/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class Namespace: NSObject {

  var uid: Int = 0
  var path: String?
  var avatar: Avatar?
  
}
