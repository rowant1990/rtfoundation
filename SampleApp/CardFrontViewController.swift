//
//  CardFrontViewController.swift
//  RTFoundation
//
//  Created by Rowan on 12/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class CardFrontViewController: CardFaceViewController {
  
  override open func viewDidLoad() {
    super.viewDidLoad()
    
    // Do any additional setup after loading the view.
  }
  
  override open func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  
  @IBAction func swapController(_ sender: UIButton) {
    if let cardController = self.cardViewController {
      cardController.cardViewFlip(true)
    }
  }
  
  
}
