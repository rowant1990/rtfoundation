//
//  ControllerFlipPageViewController.swift
//  RTFoundation
//
//  Created by Rowan on 11/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class CardFlipPageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
  
  var orderedViewControllers: [UIViewController]!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.dataSource = self
    UIPageControl.appearance().backgroundColor = UIColor.darkGray
    
    if let firstViewController = orderedViewControllers.first {
      setViewControllers([firstViewController],
        direction: .forward,
        animated: true,
        completion: nil)
    }
  }

  // MARK: - UIPageViewControllerDataSource
  
  func pageViewController(_ pageViewController: UIPageViewController,
    viewControllerBefore viewController: UIViewController) -> UIViewController? {
      guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
        return nil
      }
      
      let previousIndex = viewControllerIndex - 1
      
      guard previousIndex >= 0 else {
        return nil
      }
      
      guard orderedViewControllers.count > previousIndex else {
        return nil
      }
      
      return orderedViewControllers[previousIndex]
  }
  
  func pageViewController(_ pageViewController: UIPageViewController,
    viewControllerAfter viewController: UIViewController) -> UIViewController? {
      guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
        return nil
      }
      
      let nextIndex = viewControllerIndex + 1
      let orderedViewControllersCount = orderedViewControllers.count
      
      guard orderedViewControllersCount != nextIndex else {
        return nil
      }
      
      guard orderedViewControllersCount > nextIndex else {
        return nil
      }
      
      return orderedViewControllers[nextIndex]
  }
  
  func presentationCount(for pageViewController: UIPageViewController) -> Int {
    return orderedViewControllers.count
  }
  
  func presentationIndex(for pageViewController: UIPageViewController) -> Int {
    guard let firstViewController = viewControllers?.first,
      let firstViewControllerIndex = orderedViewControllers.index(of: firstViewController) else {
        return 0
    }
    
    return firstViewControllerIndex
  }
  

}
