//
//  ChatPresenceTableViewCell.swift
//  RTFoundation
//
//  Created by Rowan on 03/03/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class ChatPresenceTableViewCell: UITableViewCell {
  
  @IBOutlet var online: UILabel!
  @IBOutlet var name: UILabel!

  override func awakeFromNib() {
    super.awakeFromNib()
    // Initialization code
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
}
