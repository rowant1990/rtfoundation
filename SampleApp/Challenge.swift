//
//  User.swift
//  RTFoundation
//
//  Created by Rowan on 03/03/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class Challenge: NSObject {
  
  var realm: String!
  var qop: String!
  var charset: String!
  var nonce: String!
  var algorithm: String!
  
  init(fromDict dict: [String: String]) {
    realm = dict["realm"]
    qop = dict["qop"]
    charset = dict["charset"]
    nonce = dict["nonce"]
    algorithm = dict["algorithm"]
  }
}
