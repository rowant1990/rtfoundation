//
//  Project.swift
//  NetworkTest
//
//  Created by Rowan on 08/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class Project: NSObject {
  
  var branch: String?
  var name: String?
  var uid: String?
  var namespace: Namespace?
  var webUrl: URL?
  var createdAt: Date?
}
