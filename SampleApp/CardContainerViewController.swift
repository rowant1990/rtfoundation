//
//  CardContainerViewController.swift
//  RTFoundation
//
//  Created by Rowan on 11/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class CardContainerViewController: UIViewController {
  
  open var cards: [CardViewController]!
  
  lazy var pageViewController: CardFlipPageViewController = {
    let contoller = CardFlipPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    return contoller
  }()
  
  override open func viewDidLoad() {
    super.viewDidLoad()
        
    self.pageViewController.orderedViewControllers = self.cards
    
    self.addChildViewController(self.pageViewController)
    self.view.addSubview(self.pageViewController.view)
    self.pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
    self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[page]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "page" : self.pageViewController.view]))
    self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[page]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "page" : self.pageViewController.view]))
    self.pageViewController.didMove(toParentViewController: self)
  }
}
