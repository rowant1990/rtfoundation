//
//  CardBackViewController.swift
//  RTFoundation
//
//  Created by Rowan on 11/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class CardBackViewController: CardFaceViewController {
  
  override open func viewDidLoad() {
    super.viewDidLoad()
    self.view.backgroundColor = UIColor.orange
    
    // Do any additional setup after loading the view.
  }
  
  override open func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  @IBAction func back(_ sender: UIButton) {
    
    if let cardController = self.cardViewController {
      cardController.cardViewFlip(false)
    }
  }
    
}
