//
//  ChatMessageView.swift
//  RTFoundation
//
//  Created by Rowan on 03/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

protocol ChatMessageViewDataSource {
  func chatCell(forRowAtIndex indexPath:IndexPath) -> UITableViewCell
}

protocol ChatMessageViewDelegate {
  
}

open class ChatMessageView: UIView {

  //MARK: Public Variables
  
  var style = UITableViewStyle.plain
  var tableView: UITableView!
  var estimatedRowHeight: CGFloat = 40
  var dataSource: ChatMessageViewDataSource?
  
  //MARK: Initializers
  
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    self.commonInitialization()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    self.commonInitialization()
  }
  
  func commonInitialization() {
    let tableView = UITableView(frame: self.frame, style: self.style)
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.estimatedRowHeight = self.estimatedRowHeight
    self.tableView = tableView
    self.addSubview(tableView)
    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[tableView]|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "tableView" : tableView ]))
    self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[tableView]|", options:NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "tableView" : tableView ]))
  }
  
  // MARK: UITableView
  
//  public func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
//    if let dataSource = self.dataSource {
//     let cell = dataSource.chatCell?(forRowAtIndex: indexPath)
//    }
//  
//    
//  }
//  
//  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//    return 0
//  }
  
  

}
