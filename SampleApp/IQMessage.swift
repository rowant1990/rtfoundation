//
//  IQMessage.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 06/04/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class IQMessage: NSObject {

  var session: String
  var jid = ""
  
  init(fromSession session: String) {
    self.session = session
  }
}
