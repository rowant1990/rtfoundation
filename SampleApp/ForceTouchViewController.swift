//
//  ForceTouchViewController.swift
//  RTFoundation
//
//  Created by Rowan on 10/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class ForceTouchViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIViewControllerPreviewingDelegate {

  @IBOutlet var tableView: UITableView!
  var dataSource = [ ["title" : "Teststing Table", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."],
  ["title" : "Teststing Table 2", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."] ,
  ["title" : "Teststing Table 3", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."] ,
  ["title" : "Teststing Table 4", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."] ,
  ["title" : "Teststing Table 5", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."] ,
  ["title" : "Teststing Table 6", "content" : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non nibh semper, pulvinar purus ut, pretium neque. Integer nec ante metus. Vivamus lorem lacus, feugiat id enim et, aliquam rhoncus ex. Morbi hendrerit faucibus eros, quis dapibus dui lacinia ut. Curabitur fermentum lacus massa, nec vestibulum enim fringilla id. Vestibulum sit amet ante vitae risus ornare mattis. Proin lobortis purus porttitor felis vulputate vehicula. Duis convallis nulla vitae justo lacinia, sit amet placerat felis accumsan."] ]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    if #available(iOS 9.0, *) {
        if self.traitCollection.forceTouchCapability == .available {
          self.registerForPreviewing(with: self, sourceView: self.tableView)
        }
    } else {
        // Fallback on earlier versions
    }
    
    // Do any additional setup after loading the view.
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let identifier = "CellIdentifier"
    var cell = tableView.dequeueReusableCell(withIdentifier: identifier)
    if cell == nil {
      cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
    }
    let data = dataSource[(indexPath as NSIndexPath).row]
    cell!.textLabel?.text = data["title"]
    return cell!
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  //MARK: Force Touch
  
  func previewingContext(_ previewingContext: UIViewControllerPreviewing, viewControllerForLocation location: CGPoint) -> UIViewController? {
    guard let indexPath = self.tableView.indexPathForRow(at: location) else { return nil }
    guard let cell = self.tableView.cellForRow(at: indexPath) else { return nil }
    let detailVc = self.storyboard?.instantiateViewController(withIdentifier: "ForceTouchDetailedViewControllerIdentifier") as! ForceTouchDetailedViewController
    let data = self.dataSource[(indexPath as NSIndexPath).row]
    detailVc.view.tag = (indexPath as NSIndexPath).row
    detailVc.view.layoutIfNeeded()
    detailVc.mainTitleLabel.text = data["title"]
    detailVc.subTitle.text = data["content"]
    
    detailVc.preferredContentSize = CGSize(width: 0.0, height: 200)
    if #available(iOS 9.0, *) {
        previewingContext.sourceRect = cell.frame
    } else {
        // Fallback on earlier versions
    }
    
    return detailVc
  }
  
  func previewingContext(_ previewingContext: UIViewControllerPreviewing, commit viewControllerToCommit: UIViewController) {
    // Open accordion
    print("Open Row \(viewControllerToCommit.view.tag)")
    
    self.show(viewControllerToCommit, sender: self)
  }
  
  
}
