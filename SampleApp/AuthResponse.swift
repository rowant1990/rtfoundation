//
//  AuthResponse.swift
//  RTFoundation
//
//  Created by Rowan on 05/04/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

class AuthResponse: NSObject {
  
  static func compile(_ username: String, password: String, clientNonce : String, authzid: String, digestUri: String, nc: String, challenge: Challenge) -> String {
   
    let a1Hash = a1(username, realm: challenge.realm, password: password, nonce: challenge.nonce, clientNonce: clientNonce, authzid: authzid)
    let a2Hash = a2(digestUri)
    
    let kdString = "\(a1Hash):\(challenge.nonce):\(nc):\(clientNonce):\(challenge.qop):\(a2Hash)"
    let final = kdString.MD5()
    return final
  }
  
  static func a1(_ username: String, realm: String, password: String, nonce: String, clientNonce: String, authzid: String) -> String {
    
    let userToken = "\(username):\(realm):\(password)"
    let hash1Data: NSMutableData = (userToken.MD5Data() as NSData).mutableCopy() as! NSMutableData
    
    let nonceData = ":\(nonce):\(clientNonce):\(authzid)".data(using: String.Encoding.utf8)
    hash1Data.append(nonceData!)
    
    return hash1Data.MD5().hexedString()
  }
  
  static func a2(_ digestURL: String) -> String {
    return  "AUTHENTICATE:\(digestURL)".MD5()
  }
    
}

extension Data {
  func hexedString() -> String {
    var string = String()
    for i in UnsafeBufferPointer<UInt8>(start: UnsafeMutablePointer<UInt8>(bytes), count: count) {
      string += Int(i).hexedString()
    }
    return string
  }
  
  func MD5() -> Data {
    let result = NSMutableData(length: Int(CC_MD5_DIGEST_LENGTH))!
    CC_MD5(bytes, CC_LONG(count), UnsafeMutablePointer<UInt8>(result.mutableBytes))
    return (NSData(data: result) as Data)
  }
  
  func SHA1() -> Data {
    let result = NSMutableData(length: Int(CC_SHA1_DIGEST_LENGTH))!
    CC_SHA1(bytes, CC_LONG(count), UnsafeMutablePointer<UInt8>(result.mutableBytes))
    return (NSData(data: result) as Data)
  }
}

extension Int {
  func hexedString() -> String {
    return NSString(format:"%02x", self) as String
  }
}

extension String {
  func MD5() -> String {
    return (self as NSString).data(using: String.Encoding.utf8.rawValue)!.MD5().hexedString()
  }
  
  func MD5Data() -> Data {
    return (self as NSString).data(using: String.Encoding.utf8.rawValue)!.MD5()
  }
  
  func SHA1() -> String {
    return (self as NSString).data(using: String.Encoding.utf8.rawValue)!.SHA1().hexedString()
  }
}
