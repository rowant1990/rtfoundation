//
//  NetworkViewController.swift
//  RTFoundation
//
//  Created by Rowan on 18/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class NetworkViewController: UIViewController {
  
  @IBOutlet private weak var tableView: UITableView! {
    didSet {
      tableView.register(UINib(nibName: String(describing: UserTableViewCell.self), bundle: nil), forCellReuseIdentifier: "UserTableViewCellIdentifier")
    }
  }
  
  var users = [User]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    
    
    //    let request = ["username" : "cc@tc.com", "password" : "travel123", "client_id" : "MobileApp", "grant_type" : "password"]
    //    let job = CombineJob(errorHandler: self)
    //    job.perform()
    
    
//      FirstJob().retrive().then { (test) in
//          return SecondJob(from: test).retreive()
//        }
//    FirstJob().retrive().then { (test) in
//      return SecondJob().retrive()
//    }
//      return QuotesSummaryPromise()
    
    
        FirstJob().retrive().then { (t) in
          return SecondJob(fromString: t).retrive()
          }.then { (t) in
            print(t)
          }
    
    
    //    FirstJob().then { (test) in
    //
    //
    //      let a = test.appending("s")
    //      return SecondJob(from: a)
    //    }.then { (final) in
    //      print(final)
    //    }
    //    //
    
//    BookingSummaryPromise().retrive().then { (test) in
//        return QuotesSummaryPromise(fromString: "").retrive()
//        }.then { (int) in
//          print(int)
//    }
    
    
    //    API<COB, BookingSummary>.request(.posts) { [weak self] (results, error) -> Void in
    //      guard let results = results else { return }
    //      print(results)
    //
    //      self?.tableView.reloadData()
    //    }
  }
}

class FirstJob {
  
  func retrive() -> Job<String> {
    return Job(work: { (finished, error) in
      API.request(COB.posts) { (results:  Result<BookingSummary>) -> Void in
        switch results {
        case .success(let data):
          finished("d")
        default:
          break
        }
      }
    })
  }
}

class SecondJob {
  
//  init(from: String) {
////    super.init { (finished, error) in
////      error(NSError(domain: "", code: 2, userInfo: nil))
////      //      finished(from + " Rowan")
////    }
//  }
  
  init(fromString: String) {
    
  }
  
  func retrive() -> Job<String> {
    return Job(work: { (finished, error) in
      finished("str")
         })
  }

}


class QuotesSummaryPromise {
  
  init(fromString: String) {
    
  }
  
  func retrive() -> Promise<Int> {
    return  Promise(work: { (finished, error) in
      finished(3)
    })
  }
}

extension NetworkViewController: UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 164
  }
}

extension NetworkViewController: UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCellIdentifier") as! UserTableViewCell
    cell.user = users[indexPath.row]
    return cell
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return users.count
  }
}

class LoginResponse: NSObject {
  
  var accessToken = ""
  var isPasswordChange = false
  var tokenType = ""
}





//struct CombineJob: Job {
//
//  weak var errorHandler: JobError?
//
//  func perform(_ outcome: ((String) -> AnyJob<Any>)? = nil) -> Void {
//    let bookingJob = BookingSummaryJob(fromError: self.errorHandler)
//    let job = bookingJob.perform { (summary) in
//      var printJob = PrintJob(fromError: self.errorHandler)
//      printJob.input = summary.bookings?.first?.id ?? ""
//      printJob.perform()
//      return AnyJob(job: printJob)
//    }
//
//  }
//
//  func cancel() {
//
//  }
//}
//
//
//final class BookingSummaryJob: RequestJob {
//
//  weak var errorHandler: JobError?
//  var dataTask: URLSessionDataTask?
//
//  @discardableResult
//  func perform(_ outcome: ((BookingSummary) -> AnyJob<Any>)?) -> Void {
//    dataTask = request { (result, error) in
//      guard let result = result, error == nil else { self.errorHandler?.failed(error: error!); return }
//      outcome?(result)
//    }
//  }
//
//  func request(result: @escaping (BookingSummary?, NSError?) -> Void) -> URLSessionDataTask? {
//    return API<COB, BookingSummary>.request(.posts) { (results, error) -> Void in
//      result(results, error)
//    }
//  }
//
//  func cancel() {
//    dataTask?.cancel()
//    errorHandler?.cancelled()
//  }
//}
//
//struct PrintJob: Job {
//
//  var input = ""
//  weak var errorHandler: JobError?
//
//
//
//  @discardableResult
//  func perform(_ outcome: ((Any) -> AnyJob<Any>)? = nil) {
//    if input == "test" {
//      errorHandler?.failed(error: NSError(domain: "", code: 1, userInfo: nil))
//    }
//    print(input)
//  }
//
//  func cancel() {
//    errorHandler?.cancelled()
//  }
//}
//
//
enum ErrorPromise: Error {
  
  case test
  
}

class Test {
  
  convenience init(work: @escaping (_ fulfill: @escaping (String) -> (), _ reject: (String) -> () ) -> Void) {
    self.init()
    //    queue.async(execute: {
    //      do {
    //        try work(self.fulfill, self.reject)
    //      } catch let error {
    //        self.reject(error)
    //      }
    //    })
  }
  
  
}
