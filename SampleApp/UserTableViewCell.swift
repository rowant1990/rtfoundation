//
//  UserTableViewCell.swift
//  RTFoundation
//
//  Created by Rowan on 24/02/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

  @IBOutlet private weak var nameLabel: UILabel!
  @IBOutlet private weak var usernameLabel: UILabel!
  @IBOutlet private weak var emailLabel: UILabel!
  @IBOutlet private weak var addressLabel: UILabel!
  @IBOutlet private weak var companyLabel: UILabel!

  var user: User! {
    didSet {
      nameLabel.text = user.name
      usernameLabel.text = user.username
      emailLabel.text = user.email
      addressLabel.text = user.url?.absoluteString
      companyLabel.text = user.website?.absoluteURL.absoluteString
    }
  }
  
}
