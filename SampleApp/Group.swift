//
//  Group.swift
//  NetworkTest
//
//  Created by Rowan on 08/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class Group: Mappable {
  
  var name: String?
  var path: String?

  required convenience init?(map: Map) {
    self.init()
  }
  
  func mapping(map: Map) {
    
  }

  
}
