//
//  APIErrorHandler.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 09/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class APIErrorHandler: NetworkErrorHandler {

  override func configureErrorWithResponse(_ response: Any) -> NSError? {
    guard let response = response as? [String: Any], let error = response["error_description"] as? String  else { return nil }
    return NSError(domain: "com.tc", code: 1, userInfo: [ NSLocalizedDescriptionKey : error ])
  }
}
