//
//  CollectionViewDataSource.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 09/02/2018.
//

import UIKit

extension TableViewDataSource: UICollectionViewDataSource {
  
  public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    let rows = pathFinder.numberOfRows(for: section)
    return rows
  }
  
  public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let blankCell = UICollectionViewCell()// UICollectionViewCell(style: .default, reuseIdentifier: "BlankCell")
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return blankCell }
    registerNib(identifier: row.identifier, for: collectionView)
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: row.identifier, for: indexPath)
    row.update(model: value, with: cell, for: indexPath)
    return cell
  }
  
  public func numberOfSections(in collectionView: UICollectionView) -> Int {
    guard let data = data else { return 0 }
    return sectionModels.count > 0 ? data.count : 1
  }
}
