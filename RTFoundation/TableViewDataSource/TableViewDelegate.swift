//
//  TableViewDelegate.swift
//  TCFoundationSwift
//
//  Created by Rowan Townshend on 16/01/2018.
//

import UIKit

open class TableViewDelegate: TableViewInteractor {
  
  public weak var scrollDelegate: UIScrollViewDelegate?
}

extension TableViewDelegate: UITableViewDelegate {
  
  public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return }
    row.selected(model: value)
  }
  
  public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
    guard let (_, row)  = pathFinder.pathRow(for: indexPath) else { return }
    row.selected(model: nil)
  }
  
  public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return UITableView.automaticDimension }
    return row.height(for: value)
  }
  
  public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    guard let (value, newSection)  = pathFinder.pathSection(for: section) else { return nil }
    let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: newSection.identifier)
    newSection.update(model: value, with: view, for: section)
    return view
  }
  
  public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    guard let (value, newSection)  = pathFinder.pathSection(for: section, type: .footer) else { return nil }
    let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: newSection.identifier)
    newSection.update(model: value, with: view, for: section)
    return view
  }
  
  
  public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    guard let (value, newSection)  = pathFinder.pathSection(for: section) else { return 0.01 }
    return newSection.height(for: value)
  }
  
  public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    guard let (value, newSection)  = pathFinder.pathSection(for: section, type: .footer) else { return 0.01 }
    return newSection.height(for: value)
  }
}

extension TableViewDelegate: UIScrollViewDelegate {
  
  public func scrollViewDidScroll(_ scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll?(scrollView)
  }
}

public enum Supplementary {
  
  case footer, header, none
}

