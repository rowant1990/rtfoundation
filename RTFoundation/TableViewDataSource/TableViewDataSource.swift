//
//  DyanimcTableViewDataSourceNew.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 08/11/2017.
//

import UIKit

open class TableViewDataSource: TableViewInteractor {
  
}

extension TableViewDataSource: UITableViewDataSource {
  
  public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let blankCell = UITableViewCell(style: .default, reuseIdentifier: nil)
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return blankCell }
    
    //    registerNib(identifier: row.identifier, for: tableView)
    let cell = tableView.dequeueReusableCell(withIdentifier: row.identifier, for: indexPath)
    row.update(model: value, with: cell, for: indexPath)
    return cell
  }
  
  public func numberOfSections(in tableView: UITableView) -> Int {
    guard let data = data else { return 0 }
    return sectionModels.count > 0 ? data.count : 1
  }
  
  public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    let rows = pathFinder.numberOfRows(for: section)
    return rows
  }
}

public protocol TableViewItemable {
  
  var typeString: String { get }
  var identifier: String { get }
  func rawValidate(model: Any) -> RowValidatable
}

public protocol RowModelProtocol: TableViewItemable {
  
  init()
  
  func selected(model: Any?)
  func update(model: Any, with view: UIView, for indexPath: IndexPath)
  func size(with model: Any, for view: UICollectionView) -> CGSize
  func height(for model: Any) -> CGFloat
}

public protocol SectionModelProtocol: TableViewItemable {
  
  init()
  
  var type: Supplementary { get }
  
  func update(model: Any, with view: UIView?, for section: Int)
  func height(for model: Any) -> CGFloat
}

public protocol RowModelConfigurable: RowModelProtocol {
  
  associatedtype T
  associatedtype Cell: UIView
  
  func configure(cell: Cell, with model: T, for indexPath: IndexPath)
  func configure(cell: Cell, with model: T)
  
  func cellSelected(with model: T?)
  func validate(model: T) -> RowValidatable
  func configureSize(with model: T, for view: UICollectionView) -> CGSize
  func configureHeight(for model: T) -> CGFloat
}

public protocol SectionModelConfigurable: SectionModelProtocol {
  
  associatedtype T
  associatedtype View: UIView
  
  func configure(view: View, with model: T, for section: Int)
  func configure(view: View, with model: T)
  func height(for model: T) -> CGFloat
  
  func validate(model: T) -> RowValidatable
}

public extension RowModelConfigurable {
  
  func height(for model: Any) -> CGFloat {
    guard let model = model as? T else { return UITableView.automaticDimension }
    return configureHeight(for: model)
  }
  
  public func size(with model: Any, for view: UICollectionView) -> CGSize {
    guard let model = model as? T else { return CGSize.zero }
    return configureSize(with: model, for: view)
  }
  
  public func selected(model: Any?) {
    cellSelected(with: model as? T)
  }
  
  public var typeString: String {
    return String(describing: T.self)
  }
  
  public var identifier: String {
    return "\(String(describing: Cell.self))Identifier"
  }
  
  func update(model: Any, with view: UIView, for indexPath: IndexPath) {
    guard let model = model as? T, let view = view as? Cell else { return }
    configure(cell: view, with: model, for: indexPath)
  }
  
  func rawValidate(model: Any) -> RowValidatable {
    guard let model = model as? T else { return .none }
    return validate(model: model)
  }
  
  func validate(model: T) -> RowValidatable {
    return .none
  }
  func configure(cell: Cell, with model: T, for indexPath: IndexPath) {
    configure(cell: cell, with: model)
  }
  func configure(cell: Cell, with model: T) { }
  func configureSize(with model: T, for view: UICollectionView) -> CGSize {
    return CGSize.zero
  }
  func cellSelected(with model: T?) { }
  
  func configureHeight(for model: T) -> CGFloat {
    return UITableView.automaticDimension
  }
}

public extension SectionModelConfigurable {
  
  var type: Supplementary {
    return .header
  }
  
  public var typeString: String {
    return String(describing: T.self)
  }
  
  public var identifier: String {
    return "\(String(describing: View.self))Identifier"
  }
  
  func height(for model: Any) -> CGFloat {
    guard let model = model as? T else { return 0 }
    return height(for: model)
  }
  
  func update(model: Any, with view: UIView?, for section: Int) {
    guard let model = model as? T, let view = view as? View else { return }
    configure(view: view, with: model, for: section)
  }
  
  func rawValidate(model: Any) -> RowValidatable {
    guard let model = model as? T else { return .none }
    return validate(model: model)
  }
  
  func validate(model: T) -> RowValidatable {
    return .none
  }
  
  func configure(view: View, with model: T, for section: Int) {
    configure(view: view, with: model)
  }
  func configure(view: View, with model: T) { }
  
  func height(for model: T) -> CGFloat {
    return 0.01
  }
}


public protocol DataModelProtocol {
  
  var keyPath: AnyKeyPath { get }
  var typeString: String { get }
}

public enum RowValidatable {
  
  case pass, fail, none
}

struct SectionModel: DataModelProtocol {
  
  let classType: AnyClass
  let keyPath: AnyKeyPath
  let typeString: String
  
  init(withClass type: AnyClass, keyPath: AnyKeyPath) {
    self.classType = type
    self.keyPath = keyPath
    typeString = String(describing: type)
  }
}

