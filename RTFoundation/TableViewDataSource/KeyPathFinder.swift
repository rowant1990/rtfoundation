//
//  DyanimcTableViewDataSourceNew.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/11/2017.
//

import UIKit

typealias SectionType = (Any, SectionModelProtocol)?

class PathSection {
  
  init(with rows: [PathRow], header: SectionType?, footer: SectionType?) {
    self.rows = rows
    self.footerSection = footer
    self.headerSection = header
  }
  
  let headerSection: SectionType?
  let footerSection: SectionType?
  let rows: [PathRow]
}

class PathRow {
  
  init(with value: Any, row: RowModelProtocol) {
    self.value = value
    self.row = row
  }
  
  let value: Any
  let row: RowModelProtocol
}

class KeyPathFinder {
  
  var rows: [RowModelProtocol]?
  var sections: [SectionModelProtocol]?
  var sectionModels: [DataModelProtocol] = []
  var data: [Any]?
  var pathSections: [PathSection] = []
  
  func create()  {
    guard let data = data else { return }
    if sectionModels.count > 0 {
      var pathSections: [PathSection] = []
      for (index, _) in data.enumerated() {
        if let rows = self.rows(for: index) {
          var pathRows: [PathRow] = []
          for (rIndex, _) in rows.enumerated() {
            guard let (value, row)  = self.row(for: IndexPath(row: rIndex, section: index)) else { return }
            pathRows.append(PathRow(with: value, row: row))
          }
          if let (header, footer) = self.section(for: index) {
            pathSections.append(PathSection(with: pathRows, header: header, footer: footer))
          } else {
            pathSections.append(PathSection(with: pathRows, header: nil, footer: nil))
          }
        }
      }
      self.pathSections = pathSections
    } else {
      var pathSections: [PathSection] = []
      var pathRows: [PathRow] = []
      for (index, _) in data.enumerated() {
        guard let (value, row)  = self.row(for: IndexPath(row: index, section: 0)) else { return }
        pathRows.append(PathRow(with: value, row: row))
      }
      pathSections.append(PathSection(with: pathRows, header: nil, footer: nil))
      self.pathSections = pathSections
    }
  }
  
  func pathRow(for indexPath: IndexPath) -> (Any, RowModelProtocol)? {
    guard indexPath.section < pathSections.count else { return nil }
    let pathSectionRows = pathSections[indexPath.section].rows
    guard indexPath.row < pathSectionRows.count else { return nil }
    let pathRow = pathSectionRows[indexPath.row]
    return (pathRow.value, pathRow.row)
  }
  
  func pathSection(for section: Int, type: Supplementary = .header) -> (Any, SectionModelProtocol)? {
    guard section < pathSections.count else { return nil }
    let pathSection = pathSections[section]
    if let header = pathSection.headerSection, type == .header {
      return header
    } else if let footer = pathSection.footerSection, type == .footer {
      return footer
    }
    return nil
  }
  
  func numberOfRows(for section: Int) -> Int {
    guard section < pathSections.count else { return 0 }
    let s = pathSections[section]
    return s.rows.count
  }
  
  private func rows(for section: Int) -> [Any]? {
    guard let data = data else { return nil }
    guard section < data.count else { return nil }
    guard sectionModels.count > 0 else { return data }
    let newSection = data[section]
    guard let sectionModel = sectionModel(for: section) else { return nil }
    guard let values = newSection[keyPath: sectionModel.keyPath] as? [Any] else { return nil }
    return values
  }
  
  private func sectionModel(for index: Int) -> DataModelProtocol? {
    guard let data = data else { return nil }
    let dataSection = data[index]
    let classType = String(describing: type(of: dataSection))
    guard let section = sectionModels.filter({$0.typeString == classType }).first else { return nil }
    return section
  }
  
  
  
  private func row(for indexPath: IndexPath) -> (Any, RowModelProtocol)? {
    guard  let newRows = rows(for: indexPath.section), indexPath.row < newRows.count else { return nil }
    let newRow = newRows[indexPath.row]
    let rowType = String(describing: type(of: newRow))
    let filteredRows = rows!.filter({$0.typeString == rowType })
    guard filteredRows.count > 1 else { return filteredRows.count != 0 ? (newRow, filteredRows.first!) : nil }
    var defaultRow: RowModelProtocol?
    for filteredRow in filteredRows {
      let validate = filteredRow.rawValidate(model: newRow)
      if validate == .pass {
        return (newRow, filteredRow)
      } else {
        if validate == .none {
          defaultRow = filteredRow
        }
      }
    }
    guard let defaultRowCons = defaultRow else { return nil }
    return (newRow, defaultRowCons)
  }
  
  
  private func section(for section: Int) -> (SectionType?, SectionType?)? {
    
    guard let sections = sections else { return nil }
    guard let data = data, data.count > 0 else { return nil }
    let newSection = data[section]
    let sectionType = String(describing: type(of: newSection))
    let filteredSections = sections.filter({$0.typeString == sectionType })
    
    var header: SectionType?
    var footer: SectionType?
    
    for filteredSection in filteredSections {
      let validate = filteredSection.rawValidate(model: newSection)
      if validate == .pass {
        if filteredSection.type == .footer {
          footer = (newSection, filteredSection)
        } else {
          header = (newSection, filteredSection)
        }
      } else {
        if validate == .none {
          if filteredSection.type == .footer {
            footer = (newSection, filteredSection)
          } else {
            header = (newSection, filteredSection)
          }
        }
      }
    }
    return (header, footer)
  }
}

