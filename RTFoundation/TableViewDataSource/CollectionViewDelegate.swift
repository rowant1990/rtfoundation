//
//  CollectionViewDelegate.swift
//  ChromaColorPicker
//
//  Created by Rowan Townshend on 10/02/2018.
//

import UIKit

extension TableViewDelegate: UICollectionViewDelegateFlowLayout {
  
  public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return CGSize.zero }
    return row.size(with: value, for: collectionView)
  }
  
  public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    guard let (value, row)  = pathFinder.pathRow(for: indexPath) else { return }
    row.selected(model: value)
  }
}
