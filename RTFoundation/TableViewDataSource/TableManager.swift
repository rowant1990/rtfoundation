//
//  TableManager.swift
//  TCFoundationSwift
//
//  Created by Rowan Townshend on 17/01/2018.
//

import UIKit

public protocol TableViewManageable: class {
  
  var tableView: UITableView! { get }
}

public protocol CollectionViewManageable: class {
  
  var collectionView: UICollectionView! { get }
}

public extension CollectionViewManageable
{
  var collectionManager : TableViewManager {
    get {
      if let tableManager = objc_getAssociatedObject(self, &TableViewManagerAssociatedKey) as? TableViewManager {
        return tableManager
      }
      let tableManager = TableViewManager()
      objc_setAssociatedObject(self, &TableViewManagerAssociatedKey, tableManager, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      return tableManager
    }
    set {
      objc_setAssociatedObject(self, &TableViewManagerAssociatedKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
  }
}

private var TableViewManagerAssociatedKey = "TableViewManager Associated Key"
private var CollectionViewManagerAssociatedKey = "CollectionViewManager Associated Key"

public extension TableViewManageable
{
  var tableManager : TableViewManager {
    get {
      if let tableManager = objc_getAssociatedObject(self, &TableViewManagerAssociatedKey) as? TableViewManager {
        return tableManager
      }
      let tableManager = TableViewManager()
      objc_setAssociatedObject(self, &TableViewManagerAssociatedKey, tableManager, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
      return tableManager
    }
    set {
      objc_setAssociatedObject(self, &TableViewManagerAssociatedKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
  }
}

open class TableViewManager {
  
  public var delegate = TableViewDelegate()
  public let dataSource = TableViewDataSource()
  
  public var data: [Any]? = [] {
    didSet {
      dataSource.data = data
      delegate.data = data
    }
  }
  public weak var scrollDelegate: UIScrollViewDelegate? {
    didSet {
      delegate.scrollDelegate = scrollDelegate
    }
  }
  
  final fileprivate weak var owner: AnyObject?
  weak var tableView: UITableView? {
    if let delegate = owner as? TableViewManageable { return delegate.tableView }
    return nil
  }
  
  weak var collectionView: UICollectionView? {
    if let delegate = owner as? CollectionViewManageable { return delegate.collectionView }
    return nil
  }
  
  public init() { }
  
  public func configure(with owner: TableViewManageable) {
    self.owner = owner
    tableView?.delegate = delegate
    tableView?.dataSource = dataSource
    registerNibs(for: dataSource.rows)
    registerNibs(for: delegate.sections)
  }
  
  public func configure(with owner: CollectionViewManageable) {
    self.owner = owner
    collectionView?.delegate = delegate
    collectionView?.dataSource = dataSource
    registerNibs(for: dataSource.rows)
    registerNibs(for: delegate.sections)
  }
  
  public func register(row: RowModelProtocol) {
    dataSource.register(row: row)
    delegate.register(row: row)
  }
  
  public func register(row: RowModelProtocol.Type) {
    dataSource.register(row: row)
    delegate.register(row: row)
  }
  
  public func register(rows: [RowModelProtocol.Type]) {
    dataSource.register(rows: rows)
    delegate.register(rows: rows)
  }
  
  public func register(rows: [RowModelProtocol]) {
    dataSource.register(rows: rows)
    delegate.register(rows: rows)
  }
  
  public func registerModel(_ type: AnyClass, rows: AnyKeyPath) {
    dataSource.registerModel(type, rows: rows)
    delegate.registerModel(type, rows: rows)
  }
  
  public func registerSection(_ section: SectionModelProtocol.Type, type: Supplementary = .header) {
    delegate.registerSection(section: section)
    dataSource.registerSection(section: section)
  }
  
  public func registerSection(_ section: SectionModelProtocol, type: Supplementary = .header) {
    delegate.registerSection(section: section)
    dataSource.registerSection(section: section)
  }
}

private extension TableViewManager {
  
  func registerNibs(for rows: [RowModelProtocol]) {
    guard let tableView = tableView else { return }
    for row in rows {
      dataSource.registerNib(identifier: row.identifier, for: tableView)
    }
  }
  
  func registerNibs(for sections: [SectionModelProtocol]) {
    guard let tableView = tableView else { return }
    for section in sections {
      delegate.registerSectionNib(identifier: section.identifier, for: tableView)
    }
  }
}

