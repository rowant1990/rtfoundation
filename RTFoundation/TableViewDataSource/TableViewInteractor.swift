//
//  TableViewInteractor.swift
//  TCFoundationSwift
//
//  Created by Rowan Townshend on 17/01/2018.
//

import UIKit

open class TableViewInteractor: NSObject {
  
  public var data: [Any]? = [] {
    didSet {
      pathFinder.data = data
      pathFinder.create()
    }
  }
  var rows: [RowModelProtocol] = [] {
    didSet {
      pathFinder.rows = rows
    }
  }
  var sections: [SectionModelProtocol] = [] {
    didSet {
      pathFinder.sections = sections
      
    }
  }
  private(set) var sectionModels: [DataModelProtocol] = [] {
    didSet {
      pathFinder.sectionModels = sectionModels
    }
  }
  internal var registeredNibs: [String] = []
  
  public func registerModel(_ type: AnyClass, rows: AnyKeyPath) {
    let model = SectionModel(withClass: type, keyPath: rows)
    sectionModels.append(model)
  }
  
  public func registerSection(section: SectionModelProtocol.Type) {
    register(sections: [section.init()])
  }
  
  public func registerSection(section: SectionModelProtocol) {
    register(sections: [section])
  }
  
  public func register(row: RowModelProtocol) {
    register(rows: [row])
  }
  
  public func register(row: RowModelProtocol.Type) {
    register(rows: [row])
  }
  
  public func register(rows: [RowModelProtocol.Type]) {
    let newRows = rows.map{ $0.init() }
    register(rows: newRows)
  }
  
  public func register(rows: [RowModelProtocol]) {
    var newRows = rows
    newRows.append(contentsOf: self.rows.filter({ (row) -> Bool in
      return !newRows.contains(where: { type(of: $0) == type(of: row) })
    }))
    self.rows = newRows
  }
  
  public func register(sections: [SectionModelProtocol.Type]) {
    let newSections = sections.map({ $0.init() })
    register(sections: newSections)
  }
  
  public func register(sections: [SectionModelProtocol]) {
    var newSections = sections
    newSections.append(contentsOf: self.sections)
    self.sections = newSections
  }
  
  public func row(for indexPath: IndexPath) -> (Any, RowModelProtocol)? {
    return pathFinder.pathRow(for: indexPath)
  }
  
  lazy var pathFinder: KeyPathFinder = KeyPathFinder()
  
  internal func registerNib(identifier: String, for tableView: UITableView) {
    guard let nib = register(identifier: identifier) else { return }
    tableView.register(nib, forCellReuseIdentifier: identifier)
  }
  
  internal func registerNib(identifier: String, for collectionView: UICollectionView) {
    guard let nib = register(identifier: identifier) else { return }
    collectionView.register(nib, forCellWithReuseIdentifier: identifier)
  }
  
  @discardableResult
  internal func register(identifier: String) -> UINib? {
    guard !registeredNibs.contains(identifier) else { return nil }
    let nibName = identifier.replacingOccurrences(of: "Identifier", with: "")
    let nib = UINib(nibName: nibName, bundle: nil)
    registeredNibs.append(nibName)
    return nib
  }
  
  internal func registerSectionNib(identifier: String, for tableView: UITableView) {
    guard !registeredNibs.contains(identifier) else { return }
    let nibName = identifier.replacingOccurrences(of: "Identifier", with: "")
    let nib = UINib(nibName: nibName, bundle: nil)
    tableView.register(nib, forHeaderFooterViewReuseIdentifier: identifier)
    registeredNibs.append(nibName)
  }
}

