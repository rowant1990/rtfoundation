//
//  NSURLSessionConfiguration.swift
//  Mockingjay
//
//  Created by Kyle Fuller on 01/03/2015.
//  Copyright (c) 2015 Cocode. All rights reserved.
//

import Foundation

let swizzleDefaultSessionConfiguration: Void = {
  let defaultSessionConfiguration = class_getClassMethod(URLSessionConfiguration.self, #selector(getter: URLSessionConfiguration.default))
  let rtDefaultSessionConfiguration = class_getClassMethod(URLSessionConfiguration.self, #selector(URLSessionConfiguration.rtDefaultSessionConfiguration))
  method_exchangeImplementations(defaultSessionConfiguration!, rtDefaultSessionConfiguration!)

  let ephemeralSessionConfiguration = class_getClassMethod(URLSessionConfiguration.self, #selector(getter: URLSessionConfiguration.ephemeral))
  let rtEphemeralSessionConfiguration = class_getClassMethod(URLSessionConfiguration.self, #selector(URLSessionConfiguration.rtEphemeralSessionConfiguration))
  method_exchangeImplementations(ephemeralSessionConfiguration!, rtEphemeralSessionConfiguration!)
}()

extension URLSessionConfiguration {
  /// Swizzles NSURLSessionConfiguration's default and ephermeral sessions to add Mockingjay
  @objc public class func rtSwizzleDefaultSessionConfiguration() {
    _ = swizzleDefaultSessionConfiguration
  }

  @objc class func rtDefaultSessionConfiguration() -> URLSessionConfiguration {
    let configuration = rtDefaultSessionConfiguration()
    configuration.protocolClasses = [APIMockProtocol.self] as [AnyClass] + configuration.protocolClasses!
    return configuration
  }

  @objc class func rtEphemeralSessionConfiguration() -> URLSessionConfiguration {
    let configuration = rtEphemeralSessionConfiguration()
    configuration.protocolClasses = [APIMockProtocol.self] as [AnyClass] + configuration.protocolClasses!
    return configuration
  }
}
