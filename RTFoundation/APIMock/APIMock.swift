//
//  APIMock.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 23/01/2018.
//  Copyright © 2018 Travel Counsellors. All rights reserved.
//

import Foundation

public struct APIMock {
  
  let url: URL
  let response: String
}

public func ==(lhs: APIMock, rhs: APIMock) -> Bool {
  return lhs.url.absoluteString == rhs.url.absoluteString
}

private(set) var mocks: [APIMock] = []
private var registered = false

public class APIMockProtocol: URLProtocol {
  
  class func addMock(url: URL, response: String) -> APIMock {
    return addMock(APIMock(url: url, response: response))
  }
  
  class func addMock(_ m: APIMock) -> APIMock {
    if let exisitingMock = mock(for: m) {
      mocks.remove(at: mocks.index(where: {$0 == exisitingMock})!)
    }
    
    mocks.append(m)
    if !registered {
      URLProtocol.registerClass(APIMockProtocol.self)
    }
    return m
  }
  
  class func mock(for m: APIMock) -> APIMock? {
    return mock(equals: m.url.absoluteString)
  }
  
  class func mock(for request: URLRequest) -> APIMock? {
    return mock(equals: request.url?.absoluteString)
  }
  
  class func mock(equals: String?) -> APIMock? {
    let mock = mocks.filter { $0.url.absoluteString == equals }
    return mock.first
  }
  
}

extension APIMockProtocol {
  
  override open func startLoading() {
    if let mock = APIMockProtocol.mock(for: request) {
      
      for bundle in Bundle.allBundles {
        if let path = bundle.path(forResource: mock.response, ofType: "json"), let data = NSData(contentsOfFile: path) as Data? {
          client?.urlProtocol(self, didLoad: data)
          client?.urlProtocolDidFinishLoading(self)
          return
        }
      }
      sendError(message: "No json file was found matching \(mock.response).")
      
    } else {
      sendError(message: "Handling request without a matching stub.")
    }
  }
  public override func stopLoading() { }
  
  override public class func canInit(with request: URLRequest) -> Bool {
    return mock(for: request) != nil
  }
  
  override public class func canonicalRequest(for request: URLRequest) -> URLRequest {
    return request
  }
}

private extension APIMockProtocol {
  
  func sendError(message: String) {
    let error = NSError(domain: NSExceptionName.internalInconsistencyException.rawValue, code: 0, userInfo: [ NSLocalizedDescriptionKey: message ])
    client?.urlProtocol(self, didFailWithError: error)
  }
}
