//
//  MockingjayURLSessionConfiguration.m
//  Mockingjay
//
//  Created by Kyle Fuller on 10/05/2016.
//  Copyright © 2016 Cocode. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RTFoundation/RTFoundation-Swift.h>


@interface APIMockURLConfiguration : NSObject

@end

@implementation APIMockURLConfiguration

+ (void)load {
    [NSURLSessionConfiguration rtSwizzleDefaultSessionConfiguration];
}

@end
