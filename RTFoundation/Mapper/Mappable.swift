//
//  Mappable.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

public protocol Mappable {
  
  init?(map: Map)
  mutating func mapping(map: Map)
}

public extension Mappable {
  
  public init?(fromData data: [String: Any]) {
    if let obj: Self = Mapper().map(json: data) {
      self = obj
    } else {
      return nil
    }
  }
  
  public func toJSON() -> [String: Any] {
    return Mapper().toJSON(self)
  }
}

public extension Array where Element: Mappable {
  
  public init?(fromArray array: [[String: Any]]) {
    if let obj: [Element] = Mapper().map(array: array) {
      self = obj
    } else {
      return nil
    }
  }
}

