//
//  ErrorCode.swift
//  RTFoundation
//
//  Created by Rowan on 24/02/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

enum ErrorCode {
  
  case urlConfig
  case typeMismatch(String)
  case conversion
  case error(NSError)
  case noData
  
  var domain: String {
    return "com.networking"
  }
  
  var code: Int {
    switch self {
    case .error(let error): return error.code
    case .urlConfig: return 2
    case .typeMismatch(_): return 3
    case .conversion: return 4
    case .noData: return 5
    }
  }
  
  var message: String {
    switch self {
    case .urlConfig: return "URL is in an incorrect format"
    case .typeMismatch(let type): return "Result does not match given type \(type)"
    case .conversion: return "Error converting reponse to data."
    case .error(let error): return error.localizedDescription
    case .noData: return "No data has been returned"
    }
  }
  
  var error: NSError {
    return NSError(domain: domain, code: code, userInfo: [ NSLocalizedDescriptionKey: message ])
  }
}
