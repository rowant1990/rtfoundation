//
//  EnumTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 06/06/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

open class EnumTransform<T: RawRepresentable>: TransformType {
  
  
  public typealias Object = T
  public typealias JSON = T.RawValue
  
  public init() {}
  
  open func transform(from value: Any?) -> T? {
    if let raw = value as? T.RawValue {
      return T(rawValue: raw)
    }
    return nil
  }
  
  public func transform(to value: T?) -> T.RawValue? {
    return value?.rawValue
  }
  
}

