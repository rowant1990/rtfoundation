//
//  Map.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 27/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

public enum MappingType {
  case fromJSON
  case toJSON
}

open class Map {
  
  public var id: String? = nil
  var data: [String: Any] = [:]
  var currentValue: Any?
  public let type: MappingType
  var currentKey = ""
  
  public init(with type: MappingType = .fromJSON) {
    self.type = type
  }
  
  public subscript(string: String) -> Map {
    currentKey = string
    if type == .fromJSON {
      currentValue = valueFor(ArraySlice(string.components(separatedBy: ".")), dictionary: data)
    }
    return self
  }
  
  public func value<T>() -> T? {
    return currentValue as? T
  }
}


/// Fetch value from JSON dictionary, loop through keyPathComponents until we reach the desired object
private func valueFor(_ keyPathComponents: ArraySlice<String>, dictionary: [String: Any]) -> Any? {
  // Implement it as a tail recursive function.
  if keyPathComponents.isEmpty {
    return nil
  }
  
  if let keyPath = keyPathComponents.first {
    let object = dictionary[keyPath]
    if object is NSNull {
      return  nil
    } else if keyPathComponents.count > 1, let dict = object as? [String: Any] {
      let tail = keyPathComponents.dropFirst()
      return valueFor(tail, dictionary: dict)
    } else if keyPathComponents.count > 1, let array = object as? [Any] {
      let tail = keyPathComponents.dropFirst()
      return valueFor(tail, array: array)
    } else {
      return object
    }
  }
  
  return nil
}

/// Fetch value from JSON Array, loop through keyPathComponents them until we reach the desired object
private func valueFor(_ keyPathComponents: ArraySlice<String>, array: [Any]) -> Any? {
  // Implement it as a tail recursive function.
  
  if keyPathComponents.isEmpty {
    return nil
  }
  
  //Try to convert keypath to Int as index
  if let keyPath = keyPathComponents.first,
    let index = Int(keyPath) , index >= 0 && index < array.count {
    
    let object = array[index]
    
    if object is NSNull {
      return  nil
    } else if keyPathComponents.count > 1, let array = object as? [Any]  {
      let tail = keyPathComponents.dropFirst()
      return valueFor(tail, array: array)
    } else if  keyPathComponents.count > 1, let dict = object as? [String: Any] {
      let tail = keyPathComponents.dropFirst()
      return valueFor(tail, dictionary: dict)
    } else {
      return  object
    }
  }
  
  return nil
}

