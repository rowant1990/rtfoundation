//
//  ISO8601DateTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import Foundation

open class ISO8601DateTransform: DateFormatterTransform {

	public init() {
		let formatter = CustomDateFormatTransform(formatString: "yyyy-MM-dd'T'HH:mm:ss")
		super.init(dateFormatter: formatter.dateFormatter)
	}
	
}
