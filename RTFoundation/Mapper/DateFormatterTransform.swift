//
//  DateFormatterTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import Foundation

open class DateFormatterTransform: TransformType {
  
  public typealias Object = Date
  public typealias JSON = String
  
  public let dateFormatter: DateFormatter
  
  public init(dateFormatter: DateFormatter) {
    self.dateFormatter = dateFormatter
  }
  
  open func transform(from value: Any?) -> Date? {
    if let dateString = value as? String {
      return dateFormatter.date(from: dateString)
    }
    return nil
  }
  
  public func transform(to value: Date?) -> String? {
    guard let date = value else { return nil }
    return dateFormatter.string(from: date)
  }
}

