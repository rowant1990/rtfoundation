//
//  DateTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//
import Foundation

open class DateTransform: TransformType {
  
  
  public typealias Object = Date
  public typealias JSON = Double
  
  public init() {}
  
  open func transform(from value: Any?) -> Date? {
    if let timeInt = value as? Double {
      return Date(timeIntervalSince1970: TimeInterval(timeInt))
    }
    if let timeStr = value as? String {
      return Date(timeIntervalSince1970: TimeInterval(atof(timeStr)))
    }
    return nil
  }
  
  public func transform(to value: Date?) -> Double? {
    if let date = value {
      return Double(date.timeIntervalSince1970)
    }
    return nil
  }
}

