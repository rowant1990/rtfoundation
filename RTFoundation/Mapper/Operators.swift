//
//  Operators.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 27/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//
import Foundation

infix operator <-
infix operator >>>

//MARK: Operators

public func <- <T>(left: inout T, right: Map) {
  switch right.type {
  case .fromJSON: FromConversion.basic(object: &left, object: right.value())
  case .toJSON: left >>> right
  }
}

public func >>> <T>(left: T, right: Map) {
  if right.type == .toJSON {
    ToConversion.basicType(left, map: right)
  }
}

public func <- <T>(left: inout T?, right: Map) {
  switch right.type {
  case .fromJSON:  FromConversion.optional(object: &left, object: right.value())
  case .toJSON: left >>> right
  }
}

public func >>> <T>(left: T?, right: Map) {
  if right.type == .toJSON {
    ToConversion.optionalBasicType(left, map: right)
  }
}

public func <- <T: Mappable>(left: inout Array<T>, right: Map) {
  FromConversion.basic(array: &left, map: right)
}

public func <- <T: Mappable>(left: inout Array<T>?, right: Map) {
  FromConversion.optional(array: &left, map: right)
}

//MARK: Transform Operators

public func <- <Transform: TransformType>(left: inout Transform.Object, right: (Map, Transform)) {
  let (map, transform) = right
  switch map.type {
  case .fromJSON:
    let value = transform.transform(from: map.currentValue)
    FromConversion.basic(object: &left, object: value)
  case .toJSON:
    left >>> right
  }
}

public func >>> <Transform: TransformType>(left: Transform.Object, right: (Map, Transform)) {
  let (map, transform) = right
  if map.type == .toJSON {
    let value: Transform.JSON? = transform.transform(to: left)
    ToConversion.optionalBasicType(value, map: map)
  }
}

public func <- <Transform: TransformType>(left: inout Transform.Object?, right: (Map, Transform)) {
  let (map, transform) = right
  switch map.type {
  case .fromJSON:
    let value = transform.transform(from: map.currentValue)
    FromConversion.optional(object: &left, object: value)
  case .toJSON:
    left >>> right
  }
}

public func >>> <Transform: TransformType>(left: Transform.Object?, right: (Map, Transform)) {
  let (map, transform) = right
  if map.type == .toJSON {
    let value: Transform.JSON? = transform.transform(to: left)
    ToConversion.optionalBasicType(value, map: map)
  }
}

//MARK: Conversions

private func setValue(_ value: Any, map: Map) {
  setValue(value, key: map.currentKey, dictionary: &map.data)
}

private func setValue(_ value: Any, key: String, dictionary: inout [String : Any]) {
  dictionary[key] = value
}

internal final class ToConversion {
  
  class func basicType<N>(_ field: N, map: Map) {
    if let x = field as Any? , false
      || x is NSNumber
      || x is Bool
      || x is Int
      || x is Double
      || x is Float
      || x is String
      || x is NSNull
      || x is Array<NSNumber>
      || x is Array<Bool>
      || x is Array<Int>
      || x is Array<Double>
      || x is Array<Float>
      || x is Array<String>
      || x is Array<Any>
      || x is Array<Dictionary<String, Any>>
      || x is Dictionary<String, NSNumber>
      || x is Dictionary<String, Bool>
      || x is Dictionary<String, Int>
      || x is Dictionary<String, Double>
      || x is Dictionary<String, Float>
      || x is Dictionary<String, String>
      || x is Dictionary<String, Any>
    {
      setValue(x, map: map)
    }
  }
  
  class func optionalBasicType<N>(_ field: N?, map: Map) {
    if let field = field {
      basicType(field, map: map)
    }
  }
}

internal final class FromConversion {
  
  //MARK: Objects
  
  class func basic<FieldType>(object field: inout FieldType, object: FieldType?) {
    if let value = object {
      field = value
    }
  }
  
  class func optional<FieldType>(object field: inout FieldType?, object: FieldType?) {
    field = object
  }
  
  //MARK: Arrays
  
  class func basic<T: Mappable>(array field: inout Array<T>, map: Map) {
    if let objects: Array<T> = Mapper().map(array: map.currentValue) {
      field = objects
    }
  }
  
  class func optional<T: Mappable>(array field: inout Array<T>?, map: Map) {
    if let objects: Array<T> = Mapper().map(array: map.currentValue) {
      field = objects
    } else {
      field = nil
    }
  }
}

