//
//  NumberTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import Foundation

open class NumberTransform: TransformType {
  
  
  
  public typealias Object = String
  public typealias JSON = Int
  
  public init() {}
  
  open func transform(from value: Any?) -> String? {
    guard let value = value else { return nil }
    return String(describing: value)
  }
  
  public func transform(to value: String?) -> Int? {
    guard let string = value else { return nil }
    return Int(string)
  }
}

