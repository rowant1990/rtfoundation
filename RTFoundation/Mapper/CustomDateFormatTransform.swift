//
//  CustomDateFormatTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import Foundation

open class CustomDateFormatTransform: DateFormatterTransform {
  
  public init(formatString: String) {
    let formatter = DateFormatter()
    formatter.locale = Locale(identifier: "en_GB")
    formatter.dateFormat = formatString
    
    super.init(dateFormatter: formatter)
  }
}
