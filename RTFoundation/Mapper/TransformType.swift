//
//  TransformType.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

public protocol TransformType {
  
  associatedtype Object
  associatedtype JSON
  
  func transform(from value: Any?) -> Object?
  func transform(to value: Object?) -> JSON?
}

