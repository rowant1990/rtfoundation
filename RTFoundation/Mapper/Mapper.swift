//
//  Mapper.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

open class Mapper<Return: Mappable> {
  
  var id: String? = nil
  
  
  public init() { }
  
  public func map(json: [String: Any]) -> Return? {
    let map = Map()
    map.id = id
    map.data = json
    
    var object = Return.self.init(map: map)
    object?.mapping(map: map)
    return object
  }
  
  public func map(array: Any?) -> [Return]? {
    guard let objectArray = array as? [[String: Any]] else { return nil }
    return objectArray.compactMap(map)
  }
}

extension Mapper {
  
  public func toJSON(_ object: Return) -> [String: Any] {
    var mutableObject = object
    let map = Map(with: .toJSON)
    mutableObject.mapping(map: map)
    return map.data
  }
  
}

