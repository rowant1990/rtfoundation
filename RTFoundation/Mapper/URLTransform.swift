//
//  URLTransform.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 28/03/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

open class URLTransform: TransformType {
  
  public typealias Object = URL
  public typealias JSON = String
  
  public init() {}
  
  public func transform(from value: Any?) -> URL? {
    if let url = value as? String {
      return URL(string: url)
    }
    return nil
  }
  
  public func transform(to value: URL?) -> String? {
    return value?.absoluteString
  }
}

