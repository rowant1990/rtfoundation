//
//  Job.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 26/04/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

import UIKit

open class Job<T> {
  
  private var callbacks: [Callback<T>] = []
  private var state: State<T> = .pending

  public init() { }
  
  public init(value: T) {
    state = .fulfilled(value)
  }
  
  public init(work: @escaping (_ fulfill: @escaping (T) -> (), _ error: @escaping (NSError) -> ()) -> ()) {
    work(fullfill, reject)
  }
  
  // FLAT MAP
  @discardableResult
  public func then<New>(_ onFinished: @escaping (T) -> Job<New>) -> Job<New> {
    return Job<New>(work: { finished, error in
        self.addCallbacks({ (value) in
          onFinished(value).then(finished, error)
        }, onError: error)
    })
  }
  
  // MAP
  @discardableResult
  public func then<New>(_ onFinished: @escaping (T) -> New) -> Job<New> {
    return then({ (value) -> Job<New> in
      return Job<New>(value: onFinished(value))
    })
  }
  
  @discardableResult
  public func then(_ onFinished: @escaping (T) -> (), _ onError: @escaping (NSError) -> () = { _ in }) -> Job<T> {
    return Job<T>(work: { (finished, reject) in
      self.addCallbacks({ (value) in
        finished(value)
        onFinished(value)
      }, onError: { (error) in
        reject(error)
        onError(error)
      })
    })
  }
  
  @discardableResult
  public func `catch`(_ onRejected: @escaping (NSError) -> ()) -> Job<T> {
    return then({ _ in }, onRejected)
  }
  
  private func fullfill(_ value: T) {
    updateState(.fulfilled(value))
  }
  
  private func reject(_ error: NSError) {
    updateState(.error(error))
  }
  
  private func addCallbacks(_ onFinished: @escaping (T) -> (), onError: @escaping (NSError) -> ()) {
    let callback = Callback(onFinished: onFinished, onError: onError)
    callbacks.append(callback)
    fireCallbacksIfCompleted()
  }
  
  private func fireCallbacksIfCompleted() {
    guard !state.isPending else { return }
    callbacks.forEach { (callback) in
      switch self.state {
      case .fulfilled(let value): callback.callFinish(value)
      case .error(let error): callback.callReject(error)
      default: return
      }
    }
    callbacks.removeAll()
  }
  
  private func updateState(_ newState: State<T>) {
    state = newState
    fireCallbacksIfCompleted()
  }
}

public enum State<T> {
  
  case pending
  case fulfilled(T)
  case error(NSError)
  
  var value: T? {
    switch self {
    case .fulfilled(let value): return value
    default: return nil
    }
  }
  
  var error: NSError? {
    switch self {
    case .error(let error): return error
    default: return nil
    }
  }
  
  var isPending: Bool {
    switch self {
    case .pending: return true
    default: return false
    }
  }
}

public struct Callback<T> {
  
  let onFinished: (T) -> ()
  let onError: (NSError) -> ()
  
  func callFinish(_ value: T) {
    onFinished(value)
  }
  
  func callReject(_ error: NSError) {
    onError(error)
  }
}



//MARK: Protocol Experiment

//public protocol JobProtocol: class {
//  
//  associatedtype JobType
//  
//  var state: State<JobType> { get set }
//  var callbacks: [Callback<JobType>] { get set }
//
//  init()
//}
//
//public class AnyJob<T>: JobProtocol {
//  
//  public required init() { }
//  
//  public var callbacks: [Callback<T>] {
//    get { return _callbacks }
//    set { _callbacks = newValue }
//  }
//  public var state: State<T> {
//    get { return _state }
//    set { _state = newValue }
//  }
//
//  public typealias JobType = T
//
//  private var _state: State<T> = .pending
//  private var _callbacks: [Callback<T>] = []
//  
//
//  required public init<J: JobProtocol>(job: J) where J.JobType == T {
//    _state = job.state
//    _callbacks = job.callbacks
//  }
//}
//
//
//extension JobProtocol {
//  
//  public init(value: JobType) {
//    self.init()
//    state = .fulfilled(value)
//  }
//  
//  public func start(work: @escaping (@escaping (JobType) -> (), @escaping (NSError) -> ()) -> ()) {
//    work(fullfill, reject)
//  }
//  
//  init(work: @escaping (_ fulfill: @escaping (JobType) -> (), _ error: @escaping (NSError) -> ()) -> ()) {
//    self.init()
//    work(fullfill, reject)
//  }
//  
//  public func then<New>(_ onFinished: @escaping (JobType) -> AnyJob<New>) -> AnyJob<New> {
//    return AnyJob<New>(work: { finished, error in
//      self.addCallbacks({ (value) in
//        onFinished(value).then(finished, error)
//      }, onError: error)
//    })
//  }
//  
//  // MAP
//  @discardableResult
//  public func then<New>(_ onFinished: @escaping (JobType) -> New) -> AnyJob<New> {
//    return then({ (value) -> AnyJob<New> in
//      return AnyJob<New>(value: onFinished(value))
//    })
//  }
//  
//  @discardableResult
//  public func then(_ onFinished: @escaping (JobType) -> (), _ onError: @escaping (NSError) -> () = { _ in }) -> AnyJob<JobType> {
//    return AnyJob<JobType>(work: { (finished, reject) in
//      self.addCallbacks({ (value) in
//        finished(value)
//        onFinished(value)
//      }, onError: { (error) in
//        reject(error)
//        onError(error)
//      })
//    })
//  }
//  
//  @discardableResult
//  public func `catch`(_ onRejected: @escaping (NSError) -> ()) -> AnyJob<JobType> {
//    return then({ _ in }, onRejected)
//  }
//  
//  func fullfill(_ value: JobType) {
//    updateState(.fulfilled(value))
//  }
//  
//  func reject(_ error: NSError) {
//    updateState(.error(error))
//  }
//
//  func updateState(_ newState: State<JobType>) {
//    self.state = newState
//   fireCallbacksIfCompleted()
//  }
//  
//  func addCallbacks(_ onFinished: @escaping (JobType) -> (), onError: @escaping (NSError) -> ()) {
//    let callback = Callback(onFinished: onFinished, onError: onError)
//    callbacks.append(callback)
//    fireCallbacksIfCompleted()
//  }
//  
//  func fireCallbacksIfCompleted() {
//    guard !state.isPending else { return }
//    callbacks.forEach { (callback) in
//      switch self.state {
//      case .fulfilled(let value): callback.callFinish(value)
//      case .error(let error): callback.callReject(error)
//      default: return
//      }
//    }
//    callbacks.removeAll()
//  }
//}


