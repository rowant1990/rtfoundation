//
//  UIHelper.swift
//  RTFoundation
//
//  Created by Rowan on 25/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Foundation
import UIKit

public extension UIColor {
  
  convenience init(rgb: [CGFloat]) {
    
    if rgb.count > 4 || rgb.count < 3 {
      self.init(red: 0, green: 0, blue: 0, alpha: 0)
    } else {
      let red = rgb[0] / 255
      let green = rgb[1] / 255
      let blue = rgb[2] / 255
      let alpha = rgb.count == 3 ? 1 : rgb[3]
      self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
  }

  convenience init(hex: String) {
    
    var cString:String = hex.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
      cString = cString.substring(from: cString.index(cString.startIndex, offsetBy: 1))
    }
    if (cString.characters.count != 6) {
      self.init(red: 0, green: 0, blue: 0, alpha: 0)
    } else {
      
      let rString = cString.substring(to: cString.index(cString.startIndex, offsetBy: 2))
      let gString = (cString as NSString).substring(with: NSRange(location: 2, length: 2))
      let bString = (cString as NSString).substring(with: NSRange(location: 4, length: 2))
      
      var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
      Scanner(string: rString).scanHexInt32(&r)
      Scanner(string: gString).scanHexInt32(&g)
      Scanner(string: bString).scanHexInt32(&b)
      
      self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
  }
}

public extension UIImage {
  
  convenience init(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
    let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
    UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
    color.setFill()
    UIRectFill(rect)
    let image = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    self.init(cgImage: image!.cgImage!)
  }
}

public extension UIView {
  
  public class func loadFromNib<T>(_ owner: AnyObject? = nil, bundle: Bundle? = nil) -> T? {
    return UINib( nibName: String(describing: self), bundle: bundle).instantiate(withOwner: owner, options: nil).first as? T
  }
}

public extension UILabel {
  
  public func addLineHeight(_ height: CGFloat, content: String) {
    text = content
    addLineHeight(height)
  }
  
  public func addLineHeight(_ height: CGFloat) {
    guard let text = text else { return }
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.lineSpacing = height
    paragraphStyle.alignment = .center
    let attrString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.paragraphStyle : paragraphStyle])
    attributedText = attrString
  }
  
  public func addKerning(size: CGFloat) {
    if let text = text {
      attributedText = NSMutableAttributedString(string: text, attributes: [ NSAttributedStringKey.kern : size ])
    }
  }
  
}
