//
//  ViewBase.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 21/03/2017.
//  Copyright © 2017 Travel Counsellors. All rights reserved.
//

import UIKit

public protocol ViewSetupable: class {
  
  var isSetup: Bool { get set }
  
  func baseInit()
  func setup()
}

extension ViewSetupable {
  func setup() { }
  func baseInit() {  }
}

extension ViewSetupable {
  
  func setUpIfNeeded() {
    if !isSetup {
      isSetup = true
      setup()
    }
  }
}


open class ViewBase: UIView, ViewSetupable {
  
  public var isSetup = false
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    baseInit()
  }
  
  required public init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    baseInit()
  }
  
  override open func awakeFromNib() {
    super.awakeFromNib()
    setUpIfNeeded()
  }
  
  override open func layoutSubviews() {
    super.layoutSubviews()
    setUpIfNeeded()
  }
  
  public func setup() { }
  public func baseInit() {  }
}

