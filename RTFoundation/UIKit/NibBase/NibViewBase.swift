//
//  NibViewBase.swift
//  Travel Itinerary
//
//  Created by Rowan Townshend on 23/03/2017.
//  Copyright © 2017 Travel Counsellors. All rights reserved.
//

import UIKit

open class NibViewBase: ViewBase {
  
  @IBOutlet weak var view: UIView!
  
  override public var isSetup: Bool {
    get { return true } set {}
  }
  
  override open func baseInit() {
    Bundle.main.loadNibNamed(String(describing: type(of: self)), owner: self, options: nil)
    loadNib(intoView: view)
  }
}

extension UIView {
  
  func loadNib(intoView view: UIView) {
    view.translatesAutoresizingMaskIntoConstraints = false
    addSubview(view)
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "view": view]))
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[view]-0-|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: [ "view": view]))
  }
}


