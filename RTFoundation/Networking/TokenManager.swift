//
//  TokenManager.swift
//  TCFoundationSwift
//
//  Created by Rowan Townshend on 11/12/2017.
//  Copyright © 2017 Travel Counsellors. All rights reserved.
//

import UIKit

public protocol TokenManagable {
  
  var token: Token? { get set }
  func refresh()
}

public class TokenManager: TokenManagable {
  
  public var token: Token?
  
  public init(withToken token: String) {
    self.token = Token(refresh: nil, access: token)
  }
  
  public func refresh() {
  }
}

public struct Token {
  
  public var refresh: String?
  public var access: String?
}

