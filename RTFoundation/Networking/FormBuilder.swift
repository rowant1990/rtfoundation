//
//  FormBuilder.swift
//  RTFoundation
//
//  Created by Rowan on 29/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class FormBuilder<T>: NSObject  {
  
  var json: AnyObject!
  var mappingPath: String!
  
  open class func buildForm(fromJSON json: AnyObject, mappingPath: String? = nil, root: Mappable.Type? = nil) -> T {
    
    var mapping: NetworkMappingConfig? = nil
    if let mappingPath = mappingPath {
      mapping = NetworkMappingConfig(fromPath: mappingPath)
    } else if let root = root {
      mapping = NetworkMappingConfig(fromRoot: root)
    }
    
    let request = LocalAPIRequest<T>(fromJSON: json, mapping: mapping!)
    var nResponse:T?
    request.sendRequest { (response, error) -> Void in
      nResponse = response
    }
    return nResponse!
  }
  
  open class func buildForm(fromPlist plist: String, mappingPath: String? = nil, root: Mappable.Type? = nil) -> T {
    
    let json = Plist.formObjectsFrom(path: plist)
    return buildForm(fromJSON: json, mappingPath: mappingPath, root: root)
  }
  
}
