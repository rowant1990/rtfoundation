//
//  NetworkRequestConfig.swift
//  RTFoundation
//
//  Created by Rowan on 21/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Foundation

open class NetworkRequestConfig: NSObject {
  
  open var mappingConfig: NetworkMappingConfig?
  open var requestBuilder: NetworkRequestModifier?
  open var responseHandler: NetworkResponseHandler?
  open var errorClient: NetworkErrorHandler?
  open var request: URLRequest?
  open var errorMessages: [Int : String]?
  
  public init(fromRequest request: URLRequest? = nil, mapping: NetworkMappingConfig? = nil) {
    self.request = request
    mappingConfig = mapping
  }
}
