//
//  NetworkAPIRequest.swift
//  RTFoundation
//
//  Created by Rowan on 21/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class NetworkAPIRequest<T>: NSObject {
  
  open var config: NetworkRequestConfig?
  
  public init(fromConfig config: NetworkRequestConfig) {
    self.config = config
  }
  
  public convenience init(fromRequest request: URLRequest) {
    self.init(fromRequest: request, mapping: nil)
  }
  
  public init(fromRequest request: URLRequest, mapping: NetworkMappingConfig?) {
    let config = NetworkRequestConfig(fromRequest: request, mapping: mapping)
    self.config = config
  }
  
  open func sendRequest(_ completion: @escaping (T?, NSError?) -> Void) -> URLSessionDataTask? {
    guard let config = config, let request = config.request else { return nil }
    if let requestBuilder = config.requestBuilder {
      config.request = requestBuilder.configureRequest(request)
    }
    return NetworkAPI.sharedInstance.sendRequest(config, completion: { (response, error) -> Void in
      let response = response as? T
      if response != nil {
        completion(response, error)
      } else {
        let newError = error ?? ErrorCode.typeMismatch(String(describing: T.self)).error
        completion(response, newError)
      }
    })
  }
  
  open func cancel() {
    NetworkAPI.sharedInstance.cancelRequest(config!)
  }
  
}
