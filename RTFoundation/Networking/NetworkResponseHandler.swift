//
//  NetworkResponseHandler.swift
//  Pods
//
//  Created by Rowan on 27/07/2016.
//
//

import UIKit

open class NetworkResponseHandler: NSObject {
  
  open func handleRepsonse(_ response: URLResponse?) {
    assert(false, "This is an abstract class and this method should be overridden")
  }

}
