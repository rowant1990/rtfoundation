//
//  NetworkMappingConfig.swift
//  RTFoundation
//
//  Created by Rowan on 07/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Foundation
import CoreData

open class NetworkMappingConfig: NSObject {
  
  open var keyPath: String?
  open var context: NSManagedObjectContext?
  open var path: String
  open var root: Mappable.Type?

  public init(fromPath path: String) {
    self.path = path
  }
  
  public convenience init(fromRoot root: Mappable.Type?) {
    self.init(fromPath: "")
    self.root = root
  }
    
}
