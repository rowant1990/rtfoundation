//
//  APIConfig.swift
//  Pods
//
//  Created by Rowan on 01/03/2016.
//
//

import UIKit

public enum Result<T> {
  case success(T)
  case failure(NSError)
  
  public var error: NSError? {
    switch self {
    case .success:
      return nil
    case let .failure(response):
      return response
    }
  }
  
  public init(withResult result: T?, error: NSError?) {
    if let error = error {
      self = Result.failure(error)
    } else if let result = result {
      self = Result.success(result)
    } else {
      self = .failure(NSError(domain: "com.mytc", code: -1, userInfo: nil))
    }
  }
}

public protocol ApiConfigurable {
  
  associatedtype Endpoint: APIConfigType
  associatedtype ReturnType
  
  static func request(_ target: Endpoint, completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask?
  static func request(_ target: Endpoint, parameters: [String: Any], completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask?
  
}

public extension ApiConfigurable {
  
  @discardableResult
  static func request(_ target: Endpoint, completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask? {
    return API.request(target, completion: completion)
  }
  
  static func request(_ target: Endpoint, parameters: [String: Any], completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask? {
    return API.request(target, parameters: parameters, completion: completion)
  }
  
}

open class API {
  
  @discardableResult
  open class func request<EndPoint: APIConfigType, ReturnType>(_ target: EndPoint, parameters: [String: Any]? = nil, completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask? {
    let api = API()
    return api.request(target, parameters: parameters, completion: completion)
  }
  
  public static func addTokenManager(_ manager: TokenManagable) {
    ApiSettings.shared.tokenManager = manager
  }
  
  @discardableResult
  open func request<EndPoint: APIConfigType, ReturnType>(_ target: EndPoint, parameters: [String: Any]? = nil, completion: @escaping (Result<ReturnType>) -> Void) -> URLSessionDataTask? {
    guard let url = URL(string: "\(target.baseURL)\(target.path)") else { completion(Result(withResult: nil, error: ErrorCode.urlConfig.error)); return nil }
    var request = URLRequest(url: url)
    request.httpMethod = target.method
    
    let body = target.paramerters ?? parameters
    if let body = body {
      request.httpBody = try! JSONSerialization.data(withJSONObject: body, options: .prettyPrinted)
    }
    if let headerFields = target.headerFields {
      for (key, value) in headerFields {
        request.setValue(value, forHTTPHeaderField: key)
      }
    }
    
    let config = NetworkRequestConfig(fromRequest: request as URLRequest, mapping: nil)
    config.errorClient = target.errorClient
    config.mappingConfig = target.mapping
    config.requestBuilder = target.requestModifier
    config.responseHandler = target.responseHandler
    
    let apiRequest = NetworkAPIRequest<ReturnType>(fromConfig: config)
    return apiRequest.sendRequest({ (result, error) in
      completion(Result(withResult: result, error: error))
    })
  }
}

public protocol APIConfigType {
  
  var baseURL: String { get }
  var path: String { get }
  var method: String { get }
  var headerFields: [String: String]? { get }
  var errorClient: NetworkErrorHandler? { get }
  var responseHandler: NetworkResponseHandler? { get}
  var requestModifier: NetworkRequestModifier? { get }
  var mapping: NetworkMappingConfig? { get }
  var paramerters: [String: Any]? { get }
  var token: String? { get set }
}

public extension APIConfigType {
  
  var headerFields: [String: String]? {
    return nil
  }
  var responseHandler: NetworkResponseHandler? {
    return nil
  }
  var paramerters: [String: Any]? {
    return nil
  }
  var requestModifier: NetworkRequestModifier? {
    return nil
  }
  var errorClient: NetworkErrorHandler? {
    return nil
  }
  var mapping: NetworkMappingConfig? {
    return nil
  }
  var token: String?{
    get { return ApiSettings.shared.tokenManager?.token?.access }
    set { token = newValue }
  }
}



class ApiSettings {
  
  static let shared = ApiSettings()
  
  var tokenManager: TokenManagable?
}


