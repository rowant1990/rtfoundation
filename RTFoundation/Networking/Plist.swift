//
//  Plist.swift
//  CardOneBanking
//
//  Created by Rowan on 29/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Foundation

public class Plist: NSObject {
  
  public class func objectsFrom(path: String, subPath: String) -> AnyObject {
    let path = Bundle.main.path(forResource: path, ofType: "plist")!
    return NSDictionary(contentsOfFile: path)![subPath]! as AnyObject
  }
  
  public class func formObjectsFrom(path: String) -> AnyObject {
    return objectsFrom(path: "Forms", subPath: path)
  }
}

