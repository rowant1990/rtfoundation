//
//  NetworkErrorHandler.swift
//  RTFoundation
//
//  Created by Rowan on 07/01/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Foundation

open class NetworkErrorHandler: NSObject  {
  
  open func configureErrorWithResponse(_ response: Any) -> NSError? {
    assert(false, "This is an abstract class and this method and should be overridden")
    return nil
  }
  
}

