//
//  LocalAPIRequest.swift
//  RTFoundation
//
//  Created by Rowan on 29/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit

open class LocalAPIRequest<T>: NetworkAPIRequest<T> {
  
  let json: Any!
  
  public init(fromJSON json:Any, mapping: NetworkMappingConfig?) {
    self.json = json
    super.init(fromConfig:  NetworkRequestConfig(fromRequest: nil, mapping: mapping))
  }
  
  @discardableResult override open func sendRequest(_ completion: @escaping (T?, NSError?) -> Void) -> URLSessionDataTask? {
    NetworkAPI.sharedInstance.map(json, config: config!, completion: { (response, error) -> Void in
      completion(response as? T, error)
    })
    return nil
  }
  
}
