//
//  NetworkAPI.swift
//  TCFoundationSwift
//
//  Created by Rowan on 07/01/2016.
//  Copyright © 2016 Degree 53. All rights reserved.
//

import Foundation

typealias NetworkCompletion = (Any?, NSError?) -> Void

open class NetworkAPI: NSObject {
  
  public static let sharedInstance = NetworkAPI()
  open var certificatePath: String?
  
  fileprivate var dataTaskCache = [NetworkRequestConfig: URLSessionDataTask]()
  
  
  //MARK: Making http request
  /*
   Creates URL session on background thread and parses the responce
   */
  func sendRequest(_ config: NetworkRequestConfig, completion: @escaping NetworkCompletion) -> URLSessionDataTask? {
    guard let request = config.request else { return nil }
    let sessionConfig = URLSessionConfiguration.default
    let session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: nil)
    let dataTask = session.dataTask(with: request, completionHandler: { [weak self] (data, response, error) -> Void in
      guard let weakSelf = self else { return }
      weakSelf.parseServerData(config, data: data, response: response, error: error as NSError?, completion: completion)
    })
    dataTaskCache[config] = dataTask
    dataTask.resume()
    return dataTask
  }
  
  
  //MARK: Mapping Process
  /*
   Used to map local JSON not returned from and HTTP request
   */
  func map(_ json: Any, config: NetworkRequestConfig, completion: @escaping NetworkCompletion) {
    guard JSONSerialization.isValidJSONObject(json) else { completion(nil, ErrorCode.conversion.error); return }
    checkResponse(config, response: json, completion: completion)
  }
  
  /*
   Parses response. If the error code is not 200 an error is returned.
   */
  func parseServerData(_ config: NetworkRequestConfig, data: Data?, response: URLResponse?, error: NSError?, completion: @escaping NetworkCompletion) {
    config.responseHandler?.handleRepsonse(response)
    guard error == nil else { finishRequest(config, response: nil, error: ErrorCode.error(error!).error, completion: completion); return }
    
    if let httpResponse = response as? HTTPURLResponse, httpResponse.statusCode != 200 {
      if config.errorClient == nil {
        let newError = NSError(domain: "com.degree53.serverError", code: httpResponse.statusCode, userInfo: [NSLocalizedDescriptionKey : "There has been a server error"])
        finishRequest(config, response: nil, error: newError, completion: completion)
        return
      }
    }
    guard let data = data else {
      finishRequest(config, response: nil, error: ErrorCode.noData.error, completion: completion)
      return
    }
    convertResponse(config, data: data, completion: { (response, error) -> Void in
      self.finishRequest(config, response: response, error: error, completion: completion)
    })
  }
  
  /*
   Converts response from data to JSON dictoanry object. If conversion fails an error is returned.
   */
  func convertResponse(_ config: NetworkRequestConfig, data: Data, completion: @escaping NetworkCompletion) {
    do {
      let dict = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions(rawValue: 0))
      self.checkResponse(config, response: dict, completion: completion)
    } catch {
      completion(nil, ErrorCode.conversion.error)
    }
  }
  
  /*
   Checks error with error handler. If no error is returned then it moves on to the mapping phase if a mapping
   config exists.
   */
  func checkResponse(_ config: NetworkRequestConfig, response: Any, completion: @escaping NetworkCompletion) {
    let error = config.errorClient?.configureErrorWithResponse(response)
    if error != nil {
      completion(response, error)
    } else if config.mappingConfig != nil {
      mapResponse(config, response: response, completion: completion)
    } else {
      completion(response, error)
    }
  }
  
  /*
   Starts the mapping process
   */
  func mapResponse(_ config: NetworkRequestConfig, response: Any, completion: @escaping NetworkCompletion) {
    guard let mapping = config.mappingConfig else { return }
    
    if let root = mapping.root {
      if let response = response as? [String: Any] {
        let object = root.init(fromData: response)
        completion(object, nil)
      } else if let response = response as? [[String: Any]] {
        var objects: [Any] = []
        for object in response {
          if let o = root.init(fromData: object) {
            objects.append(o)
          }
        }
        completion(objects, nil)
      }
    } else {
      completion(response, nil)
    }
  }
  
  //MARK: Request Managment
  /*
   Finishes the request and removes it from the cache
   */
  func finishRequest(_ config: NetworkRequestConfig, response: Any?, error: NSError?, completion: @escaping NetworkCompletion) {
    self.dataTaskCache.removeValue(forKey: config)
    DispatchQueue.main.async {
      completion(response, error)
    }
  }
  
  
  func cancelRequest(_ config: NetworkRequestConfig) {
    let dataTask = self.dataTaskCache[config]
    dataTask?.cancel()
    self.dataTaskCache.removeValue(forKey: config)
  }
}


extension NetworkAPI:  URLSessionDelegate {
  
  public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
    let serverTrust = challenge.protectionSpace.serverTrust
    let credential:URLCredential = URLCredential(trust: serverTrust!)
    
    guard let certificatePath = certificatePath else {
      completionHandler(.useCredential, credential)
      return
    }
    
    let certificate = SecTrustGetCertificateAtIndex(serverTrust!, 0)
    let policies = NSMutableArray();
    policies.add(SecPolicyCreateSSL(true, (challenge.protectionSpace.host as CFString?)))
    SecTrustSetPolicies(serverTrust!, policies);
    
    var result: SecTrustResultType = SecTrustResultType(rawValue: UInt32(0))!
    SecTrustEvaluate(serverTrust!, &result)
    let isServerTrusted:Bool = result == .unspecified || result == .proceed
    
    let remoteCertificateData:Data = SecCertificateCopyData(certificate!) as Data
    let pathToCert = Bundle.main.path(forResource: certificatePath, ofType: "cer")
    let localCertificate:Data = try! Data(contentsOf: URL(fileURLWithPath: pathToCert!))
    
    if (isServerTrusted && (remoteCertificateData == localCertificate)) {
      completionHandler(.useCredential, credential)
    } else {
      completionHandler(.cancelAuthenticationChallenge, nil)
    }
  }
}

