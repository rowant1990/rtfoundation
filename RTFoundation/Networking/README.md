RTFoundation/Networking
=================

The *Networking* submodule of *RTFoundation* allows you to easily dispatch network requests and adapt them into their model counterparts within your application. 

## Installation

To grab all of *RTFoundation*, simply include the root Pod in your application’s `Podfile`:

```objective-c
pod ‘RTFoundation’, :git => ‘git@bitbucket.org:rowant1990/rtfoundation.git’
```

Alternatively if you just want the *Networking* component and its dependencies, add this instead:

```objective-c
pod ‘RTFoundation/Networking’, :git => ‘git@bitbucket.org:rowant1990/rtfoundation.git’
```

## Sending Requests


The new way to create requests is to use an APIConfig. In an api config all you have to do is create and enum and list each endpoint as a case of the enum. 

```swift
enum ExampleEndpoint {
  case teams(String)
  case savePot
  case friendsResults(String)
  case scores(String)
  case hand
  case friendResult(String, String)
  case stats(String, String)
}```

You can then extend that enum and conform to the ApiConfigType protocol which is where you set all the options.

```swift
var baseURL: String { return "\(URL.baseURL)/api/" }
  
  var path: String {
    switch self {
    case .teams(let uid):
      return "game21/pot/teams/\(uid)"
    case .savePot:
      return "game21/pot"
    case .friendsResults(let uid):
      return "game21/scores/friends/\(uid)"
    case .scores(let uid):
      return "game21/scores/\(uid)"
    case .hand:
      return "game21/hand"
    case .friendResult(let handId, let friendId):
      return "game21/scores/\(handId)/\(friendId)"
    case .stats(let handId, let teamId):
      return "fixtureByGame21HandIdAndTeamId/\(handId)/\(teamId)"
    }
  }
  
  var headerFields: [String: String]? { return ["Content-Type" : "application/json"]}
  var method: String {
    switch self {
    case .savePot:
      return "POST"
    default:
      return "GET"
    }
  }
  
  var errorClient: NetworkErrorHandler? {
    return APIErrorHandler()
  }
  
  var mapping: NetworkMappingConfig? {
    var mapping = ""
    switch self {
    case .teams(_): mapping = "PotTeams"
      break
    case .friendsResults(_): mapping = "FriendResults"
      break
    case .scores(_): mapping = "Hit21Scores"
      break
    case .hand: mapping = "Hand"
      break
    case .friendResult(_, _): mapping = "FriendScores"
      break
    case .stats(_, _): mapping = "Stats"
    default:
      return nil
    }
    return NetworkMappingConfig(fromPath: mapping)
  }
  
  var requestModifier: NetworkRequestModifier? {
    return TokenModifier(withToken: User.activeUser()?.token)
  }
  
  var responseHandler: NetworkResponseHandler? {
    return TokenResponse()
  }
```

Then all that's left to do is create a class which subclasses APIConfig so you can pass through your new enum.

```swift
class ExampleAPI<T>: APIConfig<T> {
  class func request(_ target: ExampleEndpoint, parameters: [String: AnyObject]? = nil, completion: @escaping (T?, NSError?) -> Void) {
    super.request(target, parameters: parameters, completion: completion)
  }
}```

Finally to make a request using your new API. All you need to do is pass though the correct enum case. 

```swift
    ExampleAPI<Login>.request(.hand, completion: completion)
```

## Contributing

This module is still under development and will most likely change significantly between releases. Fix bugs as you find them, and submit pull requests for API changes.
