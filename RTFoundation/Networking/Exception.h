//
//  Exception.h
//  RTFoundation
//
//  Created by Rowan on 27/02/2017.
//  Copyright © 2017 Rowan Townshend. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_INLINE NSException * _Nullable tryBlock(void(^_Nonnull tryBlock)(void)) {
  @try {
    tryBlock();
  }
  @catch (NSException *exception) {
    return exception;
  }
  return nil;
}


