fastlane documentation
================
# Installation
```
sudo gem install fastlane
```
# Available Actions
## iOS
### ios build
```
fastlane ios build
```

### ios test
```
fastlane ios test
```

### ios uat
```
fastlane ios uat
```

### ios git
```
fastlane ios git
```

### ios commit_build
```
fastlane ios commit_build
```

### ios profile
```
fastlane ios profile
```

### ios xcode
```
fastlane ios xcode
```

### ios sumbit_hockey
```
fastlane ios sumbit_hockey
```


----

This README.md is auto-generated and will be re-generated every time to run [fastlane](https://fastlane.tools).  
More information about fastlane can be found on [https://fastlane.tools](https://fastlane.tools).  
The documentation of fastlane can be found on [GitHub](https://github.com/fastlane/fastlane).