//
//  CardFlipPageViewControllerSpec.swift
//  RTFoundation
//
//  Created by Rowan on 15/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Quick
import Nimble
import UIKit
@testable import SampleApp

class CardFlipPageViewControllerSpec: QuickSpec {
  
  override func spec() {
    
    context("when choosing the next controller") {
      
      var sut: CardFlipPageViewController!
      let mockControllers = [UIViewController(), UIViewController(), UIViewController()]
      let mockPageViewController = UIPageViewController()
      
      beforeEach {
        sut = CardFlipPageViewController()
        sut.orderedViewControllers = mockControllers
      }
      
      it("it should return nil when no view controller is passed in") {
        let next = sut.pageViewController(mockPageViewController, viewControllerAfter: UIViewController())
        expect(next).to(beNil())
      }
      
      it("it should pick the correct controller") {
       
         let next = sut.pageViewController(mockPageViewController, viewControllerAfter: mockControllers.first!)
       // expect(next).to(mockControllers[1])
      }
      
    }
    
  }

}
