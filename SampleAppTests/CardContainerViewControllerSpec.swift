//
//  CardContainerViewControllerSpec.swift
//  RTFoundation
//
//  Created by Rowan on 15/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import Quick
import Nimble
@testable import SampleApp

class CardContainerViewControllerSpec: QuickSpec {
  
  override func spec() {
    
    let sut = CardContainerViewController()
    sut.cards = cards()
    
    context("when setting up a container") {
      sut.viewDidLoad()
      
      it("it should correctly assign the the cards") {
        expect(sut.cards).toNot(beNil())
        expect(sut.pageViewController.orderedViewControllers).toNot(beNil())
      }
    }
  }
  
  func cards() -> [CardViewController] {
    var cards = [CardViewController]()
    let contoller = CardViewController()
    contoller.frontController = CardFrontViewController()
    contoller.backController = CardBackViewController()
    cards.append(contoller)
    return cards
  }
}
