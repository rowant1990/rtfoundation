//
//  MockNetworkTest.swift
//  SampleAppTests
//
//  Created by Rowan Townshend on 26/01/2018.
//  Copyright © 2018 Travel Counsellors. All rights reserved.
//


import UIKit
import XCTest
import RTFoundation

class MockNetworkTestSpec: XCTestCase {
  
  func test_network() {
    mock(URL(string: "https://jsonplaceholder.typicode.com/posts/1")!, to: "LoginMock")
    
    API.request(TestAPI.test) { (result: Result<Any>) in
      switch result {
      case .success(let data):
        if let data = data as? [String: Any] {
          XCTAssertNotNil(data["access_token"])
        }
        print(data)
      case .failure(let error):
        print(error.localizedDescription)
      }
    }
  }
}

enum TestAPI {
  case test
}

extension TestAPI: APIConfigType {
  
  var baseURL: String { return "https://jsonplaceholder.typicode.com" }
  
  var path: String {
    switch self {
    case .test: return "/posts/1"
    }
  }
  
  var method: String {
    return "GET"
  }
  
}

