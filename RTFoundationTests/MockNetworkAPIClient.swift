//
//  MockNetworkAPIClient.swift
//  RTFoundation
//
//  Created by Rowan on 15/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
@testable import RTFoundation

class MockNetworkAPIClient: NetworkAPI {
  
  var parseJSONDataInvoked = false
  var checkResponseInvoked = false
  var mapResponseInvoked = false
  var errorFound = false
  
  override func finishRequest(config: NetworkRequestConfig, response: AnyObject?, error: NSError?, completion: NetworkCompletion) {
    errorFound = error != nil
  }
  
  override func convertResponse(config: NetworkRequestConfig, data: NSData, completion: NetworkCompletion) {
    super.convertResponse(config, data: data, completion: completion)
    parseJSONDataInvoked = true
  }
  
  override func checkResponse(config: NetworkRequestConfig, response: AnyObject, completion: NetworkCompletion) {
    super.checkResponse(config, response: response, completion: completion)
    checkResponseInvoked = true
  }
  
  override func mapResponse(config: NetworkRequestConfig, response: AnyObject, completion: NetworkCompletion) {
    mapResponseInvoked = true
  }
}

