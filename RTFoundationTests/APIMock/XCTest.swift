//
//  XCTest.swift
//  RTFoundation
//
//  Created by Rowan Townshend on 23/01/2018.
//  Copyright © 2018 Travel Counsellors. All rights reserved.
//

import ObjectiveC
import XCTest


extension XCTest {
  
  public func mock(_ url: URL, to response: String) -> APIMock {
    return APIMockProtocol.addMock(url: url, response: response)
  }
}
