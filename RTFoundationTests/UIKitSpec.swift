import Quick
import Nimble
import RTFoundation

class UIKitSpec: QuickSpec {
  override func spec() {
    
    context("when using UIColor helper") {
      
      var sut = UIColor()
      
      describe("with the rgb converter") {
        
        it("should be able to generate a colour") {
          
          sut = UIColor(red: 10/255, green: 25/255, blue: 10/255, alpha: 1)
          expect(sut).to(equal(UIColor(rgb: [10, 25, 10])))
        }
        
        it("should be able to generate a colour with alpha layer") {
          
          sut = UIColor(red: 245/255, green: 23/255, blue: 135/255, alpha: 0.5)
          expect(sut).to(equal(UIColor(rgb: [245, 23, 135, 0.5])))
        }
        
        it("should generate an empty color if all the values are not speicifed") {
          sut = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
          expect(sut).to(equal(UIColor(rgb: [2, 1])))
          expect(sut).to(equal(UIColor(rgb: [2])))
          expect(sut).to(equal(UIColor(rgb: [354])))
        }
      }
      
      describe("when using the hex converter") {
        
        it("should be able to generate a colour") {
          sut = UIColor(red: 10/255, green: 234/255, blue: 210/255, alpha: 1)
          expect(sut).to(equal(UIColor(hex: "0AEAD2")))
        }
        
        it("should generate a empty color if we supply an invalid hex") {
          sut = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
          expect(sut).to(equal(UIColor(hex: "#5635")))
          expect(sut).notTo(equal(UIColor(hex: "#5635AF")))
        }
      }
      
    }
  }
}
