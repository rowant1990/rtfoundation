//
//  NetworkAPI.swift
//  RTFoundation
//
//  Created by Rowan on 15/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import Quick
import Nimble
import UIKit
@testable import RTFoundation

class NetworkAPISpec: QuickSpec {
  
  override func spec() {
    
    var mockSut: MockNetworkAPIClient!
    var mockConfig: NetworkRequestConfig!
    let mockCompletion = { (object: AnyObject?, error: NSError?) in  }
    
    beforeEach {
      mockSut = MockNetworkAPIClient()
      mockConfig = NetworkRequestConfig(fromRequest: URLRequest(url: <#URL#>))
    }
    
    afterEach {
      mockSut = nil
      mockConfig = nil
    }
    
    context("when handling the response from a request") {
      
      describe("when a request has been made") {
        
        let mockData = "[ {\"name\": \"Test API\"} ]".data(using: String.Encoding.utf8)
        let mockError = NSError(domain: "test", code: 0, userInfo: nil)
        
        it("it should call completion with an error if an error is returned but not convert response") {
          mockSut.parseServerData(mockConfig, data: nil, response: nil, error: mockError, completion: mockCompletion as! NetworkCompletion)
          expect(mockSut.errorFound).to(beTruthy())
          expect(mockSut.parseJSONDataInvoked).toNot(beTruthy())
        }
        
        it("it not convert data if there is no data") {
          mockSut.parseServerData(mockConfig, data: nil, response: nil, error: nil, completion: mockCompletion as! NetworkCompletion)
          expect(mockSut.errorFound).toNot(beTruthy())
          expect(mockSut.parseJSONDataInvoked).toNot(beTruthy())
        }
        
        it("it should start converting the data if there is data and no error") {
          mockSut.parseServerData(mockConfig, data: mockData, response: nil, error: nil, completion: mockCompletion as! NetworkCompletion)
          expect(mockSut.errorFound).toNot(beTruthy())
          expect(mockSut.parseJSONDataInvoked).to(beTruthy())
        }
        
        it("it shouldn't start converting the data if there is data and an error") {
          mockSut.parseServerData(mockConfig, data: mockData, response: nil, error: mockError, completion: mockCompletion as! NetworkCompletion)
          expect(mockSut.errorFound).to(beTruthy())
          expect(mockSut.parseJSONDataInvoked).toNot(beTruthy())
        }
      }
    }
    
    context("when converting the response to objects") {
      
      let mockConfig = NetworkRequestConfig(fromRequest: NSURLRequest())
      let goodJsonData = "[ {\"name\": \"Test API\"} ]".dataUsingEncoding(NSUTF8StringEncoding)
      let badJsonData = "[ {\"na \"Test API\"} ]".dataUsingEncoding(NSUTF8StringEncoding)
      
      it("it should not check response if it recieves broken json") {
        mockSut.convertResponse(mockConfig, data: badJsonData!, completion:mockCompletion)
        expect(mockSut.checkResponseInvoked).toNot(beTruthy())
      }
      
      it("it should check response if it recieves good json") {
        mockSut.convertResponse(mockConfig, data: goodJsonData!, completion:mockCompletion)
        expect(mockSut.checkResponseInvoked).to(beTruthy())
      }
    }
    

    context("when checking the response") {
      
      let mockObject = NSObject()
      
      it("it should not map the response if a mapping config is not avaliable") {
        mockSut.checkResponse(mockConfig, response: mockObject, completion: mockCompletion)
        expect(mockSut.mapResponseInvoked).toNot(beTruthy())
      }
      
      it("it should map the response if a mapping config is avaliable") {
        mockConfig.mappingConfig = NetworkMappingConfig(fromPath: "Test")
        mockSut.checkResponse(mockConfig, response: mockObject, completion: mockCompletion)
        expect(mockSut.mapResponseInvoked).to(beTruthy())
      }
      
      describe("when the config has an error client") {
        
        let mockErrorClient = MockErrorClient()
        let sut = NetworkAPI()
        let mockErrorConfig = NetworkRequestConfig(fromRequest: NSURLRequest())
        mockErrorConfig.errorClient = mockErrorClient
        
        it("it should not map a response if the error client returns and error") {
          sut.checkResponse(mockErrorConfig, response: mockObject, completion: { (object, error) -> Void in
            expect(error).toNot(beNil())
          })
          
        }
      }
      
    }
  }
  
}

