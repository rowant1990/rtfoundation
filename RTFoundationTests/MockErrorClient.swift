//
//  MockErrorClient.swift
//  RTFoundation
//
//  Created by Rowan on 15/02/2016.
//  Copyright © 2016 Rowan Townshend. All rights reserved.
//

import UIKit
import RTFoundation

class MockErrorClient: NetworkErrorHandler {
  
  override func configureErrorWithResponse(_ response: Any) -> NSError? {
    return NSError(domain: "test", code: 0, userInfo: nil)
  }

}
